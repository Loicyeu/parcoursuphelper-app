package fr.parcoursupHelper;

import fr.parcoursupHelper.model.applicant.Applicants;
import fr.parcoursupHelper.model.mark.MarkSystemRegistry;
import fr.parcoursupHelper.model.question.Questions;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.nio.file.InvalidPathException;

/**
 * Classe App.
 * C'est la classe principale de l'application. C'est elle qui est lancé en premier au démarrage.
 */
public class App extends Application {

    private static Scene scene;
    private static HostServices hostServices;
    private static Stage stage;

    private static Applicants applicants;
    private static Questions questions;
    private static MarkSystemRegistry markSystems = new MarkSystemRegistry();


    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }

    public static void setStageSize(double width, double height) {
        stage.setWidth(width);
        stage.setHeight(height);
    }

    public static void setStageSize(double width, double height, double minWidth, double minHeight) {
        stage.setWidth(width);
        stage.setHeight(height);
        stage.setMinWidth(minWidth);
        stage.setMinHeight(minHeight);
    }

    /**
     * Méthode permettant d'afficher une vue pré-chargé.
     *
     * @param parent La vue pré-chargé.
     */
    public static void setRoot(Parent parent) {
        scene.setRoot(parent);
    }

    /**
     * Méthode permettant de récupérer une vue FXML en version java pour être afficher.
     *
     * @param fxml Le nom de la vue sans le chemin, ni l'extension.
     * @return La vue prête a être affiché.
     * @throws IOException              Si une erreur I/O survient.
     * @throws IllegalArgumentException Si le nom du fichier contient une extension, ou si un chemin est spécifié.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Méthode permettant de récupérer le load d'un vue FXML.
     *
     * @param fxml Le nom du fichier sans le chemin, ni l'extension.
     * @return Le loader FXML.
     * @throws IllegalArgumentException Si le nom du fichier contient une extension, ou si un chemin est spécifié.
     */
    public static FXMLLoader getLoader(String fxml) {
        if (fxml == null || fxml.contains(".") || fxml.contains("/"))
            throw new IllegalArgumentException("La chaîne de caractères donné n'est pas conforme : " + fxml);
        return new FXMLLoader(App.class.getResource("fxml/" + fxml + ".fxml"));
    }

    /**
     * Méthode permettant de récupérer une ressource à partir d'un chemin relatif.
     *
     * @param path Le chemin relatif de la ressource souhaité.
     * @return L'URL du fichier.
     * @throws InvalidPathException Si la ressource recherché n'existe pas.
     */
    public static URL getRessources(String path) {
        URL url = App.class.getResource(path);
        if (url == null)
            throw new InvalidPathException(path, "La ressource situé au chemin donné n'existe pas ");
        return url;
    }

    /**
     * Méthode permettant d'ouvrir le site du projet en local sur le navigateur par défaut du système.
     */
    public static void openWebsite() {
        hostServices.showDocument(getRessources("website/index.html").toString());
    }

    /**
     * Méthode permettant d'ouvrir la fenêtre aboutUs.
     */
    public static void aboutUs() {
        Stage popup = new Stage();
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.initOwner(stage);
        popup.setTitle("A propos");
        popup.getIcons().add(new Image(getRessources("image/pH_logo.png").toString()));
        BorderPane borderPane = new BorderPane();

        StackPane stackPane = new StackPane();
        stackPane.setPadding(new Insets(10));
        ImageView imageView = new ImageView(new Image(App.class.getResource("image/pH_logo_no_bg.png").toString()));

        imageView.setFitHeight(200);
        imageView.setFitWidth(200);
        stackPane.getChildren().add(imageView);
        borderPane.setLeft(stackPane);

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER_LEFT);
        Label label0 = new Label("Logiciel parcoursupHelper réalisé par :");
        label0.setFont(Font.font("default", FontWeight.BOLD, 15));
        Label label1 = new Label(" - Alan BERINGUER <alan.beringuer@etu.univ-nantes.fr>");
        label1.setFont(Font.font(15));
        Label label11 = new Label(" - Milo BEAUJOIN <milo.beaujoin@etu.univ-nantes.fr>");
        label11.setFont(Font.font(15));
        Label label2 = new Label(" - Timothé CABON <timothe.cabon@etu.univ-nantes.fr>");
        label2.setFont(Font.font(15));
        Label label21 = new Label(" - Benoit DALATI <benoit.dalati@etu.univ-nantes.fr>");
        label21.setFont(Font.font(15));
        Label label22 = new Label(" - Basile DROUET <basile.drouet@etu.univ-nantes.fr>");
        label22.setFont(Font.font(15));
        Label label3 = new Label(" - Loïc HENRY <loic.henry@etu.univ-nantes.fr>");
        label3.setFont(Font.font(15));
        Label label4 = new Label(" - William MIERE <william.miere@etu.univ-nantes.fr>");
        label4.setFont(Font.font(15));
        Label label5 = new Label(" - Aimeric SORIN <aimeric.sorin@etu.univ-nantes.fr>");
        label5.setFont(Font.font(15));
        vBox.getChildren().addAll(label0, label1, label11, label2, label21, label22, label3, label4, label5);
        borderPane.setCenter(vBox);

        popup.setScene(new Scene(borderPane, 610d, 300d));
        popup.show();
    }

    /**
     * Méthode retournant la liste des candidats.
     *
     * @return La liste des candidats.
     */
    public static Applicants getApplicants() {
        return applicants;
    }

    /**
     * Méthode permettant de changer la liste de candidats.
     *
     * @param applicants La nouvelle liste de candidats.
     * @throws IllegalArgumentException Si applicants est a null.
     */
    public static void setApplicants(Applicants applicants) {
        if (applicants == null)
            throw new IllegalArgumentException("Applicants ne peut pas prendre la valeur null.");
        App.applicants = applicants;
    }

    public static MarkSystemRegistry getMarkRegistry() {
        // TODO STOCKAGE DATABASE POUR FAIRE PERSISTER UNE FOIS LES INTERFACES FERMEES
        return markSystems;
    }

    public static void setMarkSystems(MarkSystemRegistry markSystems) {
        App.markSystems = markSystems;
    }

    /**
     * Méthode retournant la liste des questions. Dans le cas ou elle serait nulle, retourne une nouvelle liste vide.
     *
     * @return La liste des questions.
     */
    public static Questions getQuestions() {
        if (questions == null) {
            questions = new Questions();
        }
        return questions;
    }

    /**
     * Méthode permettant de changer la liste des questions.
     *
     * @param questions La nouvelle liste des questions.
     */
    public static void setQuestions(Questions questions) {
        App.questions = questions;
    }

    public static void openSave() {
        try {
            Parent root = loadFXML("save");
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Sauvegarder");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(getStage());
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode appelé automatiquement par JavaFX au démarrage de l'application
     *
     * @param primaryStage La fenêtre principale de l'application.
     * @throws Exception Si une exception survient.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        hostServices = getHostServices();
        scene = new Scene(loadFXML("main"));

        stage.setTitle("parcoursupHelper");
        stage.getIcons().add(new Image(getRessources("image/pH_logo.png").toString()));
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }
}
