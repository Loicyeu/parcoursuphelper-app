package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.model.parser.FileParser;
import fr.parcoursupHelper.model.question.Question;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class QuestionParserController {

    @FXML
    private VBox checkboxesVBox;
    @FXML
    private TextField filePathField;
    @FXML
    private TextArea detailsTextArea;
    @FXML
    private Label errorLabel;
    @FXML
    private Button cancelButton;
    @FXML
    private Button applyButton;

    private CreateQuestionsController controller;
    private final Set<Question> questionList = new HashSet<>();

    @FXML
    private void openFileExplorer() {
        resetErrorLabel();
        FileChooser fileChooser = new FileChooser();
        if(!filePathField.getText().equals("")) {
            File file = new File(filePathField.getText());
            if(file.getParentFile() != null) {
                fileChooser.setInitialDirectory(file.getParentFile());
            }
        }
        try{
            File file = fileChooser.showOpenDialog(filePathField.getScene().getWindow());
            filePathField.setText(file.getPath());
        }catch (NullPointerException e) {
            //TODO better catch this exception
            e.printStackTrace();
        }
    }

    @FXML
    private void analyse() {
        resetErrorLabel();
        if(filePathField.getText().trim().length()<=0) {
            errorLabel.setText("Le chemin vers le fichier de questions est vide.");
        }else {
            try {
                File file = new File(filePathField.getText());
                FileParser fileParser = new FileParser(file);
                List<Question> questions = fileParser.parse();
                setAnalyseDetails(questions, fileParser.getExceptions());
                createCheckboxes(questions);
                applyButton.setDisable(questionList.isEmpty());
            }catch (FileNotFoundException e) {
                errorLabel.setText("La fichier spécifié n'est pas valide.");
            }
        }
    }

    @FXML
    private void close() {
        ((Stage) cancelButton.getScene().getWindow()).close();
    }

    @FXML
    private void apply() {
        resetErrorLabel();
        if(!questionList.isEmpty()) {
            controller.addQuestions(new ArrayList<>(questionList));
            close();
        }else {
            errorLabel.setText("Il faut sélectionner au moins une question à ajouter.");
        }
    }

    public void setController(CreateQuestionsController controller) {
        this.controller = controller;
    }

    private void setAnalyseDetails(List<Question> questions, List<String> exceptions) {
        detailsTextArea.setText("Questions trouvées : " + questions.size());
        detailsTextArea.appendText("\n\nErreurs trouvées : " + exceptions.size());
        for (String exception : exceptions) {
            detailsTextArea.appendText("\n\t- "+exception+"\n");
        }
    }

    private void createCheckboxes(List<Question> questions) {
        checkboxesVBox.getChildren().clear();
        for (Question question : questions) {
            CheckBox checkBox = new CheckBox(question.getName());
            checkBox.setSelected(true);
            checkBox.setPadding(new Insets(0, 0, 10, 0));
            checkBox.setOnAction(event -> {
                if(checkBox.isSelected()){
                    questionList.add(question);
                }else {
                    questionList.remove(question);
                }
            });
            questionList.add(question);
            checkboxesVBox.getChildren().add(checkBox);
        }
    }

    private void resetErrorLabel() {
        errorLabel.setText("");
    }
}
