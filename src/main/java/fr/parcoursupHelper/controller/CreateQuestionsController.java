package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.applicant.Applicant;
import fr.parcoursupHelper.model.question.AnswersQuestion;
import fr.parcoursupHelper.model.question.Question;
import fr.parcoursupHelper.model.question.QuestionRegistry;
import fr.parcoursupHelper.model.question.Questions;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * Classe CreateQuestionController.<br>
 * Contrôleur lié à la vue createQuestion
 *
 * @author Timothé CABON, Loïc HENRY
 * @version 2.0.0
 */
public class CreateQuestionsController implements Initializable {

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button openAnswerMenuButton;
    @FXML
    private Button next;
    @FXML
    private ListView<Question> viewListQuestion;
    @FXML
    private Button removeQuestion;
    @FXML
    private MenuButton addQuestion;

    private Questions questions;
    private List<Applicant> applicants;

    /**
     * Méthode d'initialisation du contrôleur.
     * Disable tous les <code>Node</code> présent sur la vue excepté la liste des questions,
     * le bouton pour ajouter une question ainsi que le bouton "précédant".
     *
     * @param url            L'URL
     * @param resourceBundle Le resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        App.setStageSize(650, 500, 650, 500);

        openAnswerMenuButton.setDisable(true);
        next.setDisable(true);
        removeQuestion.setDisable(true);

        viewListQuestion.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        viewListQuestion.getSelectionModel().selectedItemProperty().addListener(
                (ov, oldVal, newVal) -> {
                    if (newVal != null) {
                        if (newVal instanceof AnswersQuestion) {
                            openAnswerMenuButton.setText("Menu des réponses (" + ((AnswersQuestion) newVal).getAnswers().size() + ")");
                        }
                        borderPane.setCenter(newVal.getCreatingDisplayable());
                        openAnswerMenuButton.setDisable(!(newVal instanceof AnswersQuestion));
                    } else {
                        borderPane.setCenter(null);
                    }
                });

        for (String displayName : QuestionRegistry.getQuestionName()) {
            MenuItem menuItem = new MenuItem(displayName);
            menuItem.setOnAction(event -> addQuestion(QuestionRegistry.getQuestionClass(menuItem.getText())));
            addQuestion.getItems().add(menuItem);
        }

        applicants = App.getApplicants().toList();
        questions = App.getQuestions();
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "suivant" de vue createQuestion.
     * Update la question sélectionnée une dernière fois avant de passer sur la vue application
     * et envoie à la vue suivante la liste des questions et les Applicants
     *
     * @throws IOException Si un problème lié au FXML survient
     */
    @FXML
    private void next() throws IOException {
        addQuestionToApplicant();
        FXMLLoader loader = App.getLoader("application");
        Parent x = loader.load();
        App.setRoot(x);
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "précédent".<br>
     * Affiche un fenêtre pour informer l'utilisateur que si il fait "précédent"
     * il perdra les questions et réponses créées.
     */
    @FXML
    private void previous() throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Un retour à l'étape précédente écrasera toutes les questions et réponses créées.");
        alert.setContentText("Voulez-vous vraiment faire un retour ?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            FXMLLoader loader = App.getLoader("openExcel");
            Parent x = loader.load();
            App.setRoot(x);
            Question.resetInstanceNumber();
            Answer.resetInstanceNumber();
        }
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "supprimer la question".<br>
     * Supprime de la liste des questions la question sélectionnée dans la liste et modifie
     * l'état de certain <code>Node</code> présent sur la vue.
     */
    @FXML
    private void removeQuestion() {
        Question q = viewListQuestion.getSelectionModel().getSelectedItem();
        if (questions.getSize() - 1 == 0) {
            next.setDisable(true);
            removeQuestion.setDisable(true);
        }
        questions.remove(q);
        viewListQuestion.setItems(questions.toObservableList());
        viewListQuestion.refresh();
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "Ouvrir le menu des réponses".<br>
     * Affiche la vue createAnswers et donne au contrôleur de cette vue la question sélectionnée.
     * Désactive tous les <code>Node</code> de la vue createQuestion jusqu'à la fermeture de la vue createAnswers.
     */
    @FXML
    private void openAnswerMenu() {
        FXMLLoader loader = App.getLoader("createAnswers");
        try {
            Parent root = loader.load();
            CreateAnswerController controller = loader.getController();
            AnswersQuestion q = (AnswersQuestion) viewListQuestion.getSelectionModel().getSelectedItem();
            controller.setQuestion(q);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Réponses : " + q.getName());
            removeQuestion.setDisable(true);
            addQuestion.setDisable(true);
            next.setDisable(true);
            stage.showAndWait();
            openAnswerMenuButton.setText("Menu des réponses (" +
                    ((AnswersQuestion) viewListQuestion.getSelectionModel().getSelectedItem()).getAnswers().size() + ")");
            addQuestion.setDisable(false);
            removeQuestion.setDisable(false);
            next.setDisable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lorsque l'utilisateur souhaite sauvegarder son activité.<br>
     * La sauvegarde doit être interne.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    private void save(Event event) {
    }

    /**
     * Lorsque l'utilisateur souhaite sauvegarder son activité.<br>
     * La sauvegarde doit être externe.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    private void saveAs(Event event) {
    }

    /**
     * Lorsque l'utilisateur souhaite quitter l'application.<br>
     * <b>DOIT</b> demander si l'utilisateur souhaite sauvegarder son activité avant de quitter.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    private void exit(Event event) {
    }

    /**
     * Lorsque l'utilisateur souhaite avoir les crédits de l'application
     *
     * @param event L'évènement JFX.
     */
    @FXML
    private void aboutUs(Event event) {
        App.aboutUs();
    }

    /**
     * Lorsque l'utilisateur requière l'aide en ligne.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    private void onlineHelp(Event event) {
        App.openWebsite();
    }

    /**
     * Méthode permettant d'ouvrir le menu permettant d'utiliser un fichier de questions/réponses.
     *
     * @param event L'Event.
     */
    @FXML
    private void openQuestionFile(Event event) {
        FXMLLoader loader = App.getLoader("questionParser");
        try {
            Parent root = loader.load();
            QuestionParserController controller = loader.getController();
            controller.setController(this);
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Ouvrir un fichier de question");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(borderPane.getScene().getWindow());
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode permettant d'ajouter des question à la liste des questions.
     *
     * @param questionList La liste de questions a ajouter.
     */
    public void addQuestions(List<Question> questionList) {
        for (Question question : questionList) {
            questions.add(question);
        }
        viewListQuestion.setItems(questions.toObservableList());
        viewListQuestion.refresh();
        next.setDisable(false);
        removeQuestion.setDisable(false);
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "ajouter une question".
     *
     * @param clazz La class de la question a créer.
     */
    private void addQuestion(Class<?> clazz) {
        try {
            Question question = (Question) clazz.getConstructor().newInstance();
            questions.add(question);
            viewListQuestion.setItems(questions.toObservableList());
            viewListQuestion.refresh();
            next.setDisable(false);
            removeQuestion.setDisable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode permettant d'ajouter une copie des questions à chaque candidats de la liste.
     */
    private void addQuestionToApplicant() {
        for (Applicant applicant : applicants) {
            for (Question q : questions) {
                applicant.addQuestion(q.copy());
            }
        }
    }

    public void openMarkEditor() {
        FXMLLoader loader = App.getLoader("markSystemCreator");
        try {
            Parent root = loader.load();
            MarkSystemController controller = loader.getController();
            controller.setController(new MarkSystemController());
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Systèmes de notation");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(borderPane.getScene().getWindow());
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
