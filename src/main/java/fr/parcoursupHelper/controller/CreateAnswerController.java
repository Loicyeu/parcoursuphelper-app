package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.question.AnswersQuestion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Classe CreateAnswerController.<br>
 * Contrôleur lié à la vue createAnswer.
 *
 * @author Timothé CABON
 * @version 1.0.0
 */
public class CreateAnswerController implements Initializable {


    @FXML
    private ListView<Answer> viewListAnswer;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Button removeAnswer;

    private AnswersQuestion question;
    private ObservableList<Answer> answerList;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        viewListAnswer.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        viewListAnswer.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        borderPane.setCenter(newValue.getCreatingDisplayable());
                    } else {
                        borderPane.setCenter(null);
                    }
                });
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "Ajouter une réponse".
     * Ajoute une réponse dans la liste des réponses.
     */
    @FXML
    private void addAnswer() {
        Answer a = new Answer(0, question);
        if (answerList == null) {
            answerList = FXCollections.observableArrayList();
        }
        a.setUid(question.getUid());
        answerList.add(a);
        question.addAnswer(a);
        viewListAnswer.setItems(answerList);
        removeAnswer.setDisable(false);
    }

    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "supprimer la réponse".
     * Supprime la réponse de la liste des réponses.
     */
    @FXML
    private void removeAnswer() {
        Answer answer = viewListAnswer.getSelectionModel().getSelectedItem();
        if (answerList.size() - 1 == 0) {
            removeAnswer.setDisable(true);
        }
        answerList.remove(answer);
        viewListAnswer.setItems(answerList);
        question.removeAnswer(answer);
    }

    /**
     * Donne au controleur la question a la quelle toute les réponses créées vont être liées.
     *
     * @param question la question
     */
    public void setQuestion(AnswersQuestion question) {
        this.question = question;
        answerList = FXCollections.observableArrayList();
        answerList.addAll(question.getAnswers());
        viewListAnswer.setItems(answerList);
        if (answerList.isEmpty()) {
            removeAnswer.setDisable(true);
        }
    }

}
 