package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.SqliteConnexion;
import fr.parcoursupHelper.model.database.Database;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.ResourceBundle;

/**
 * Classe MainController.<br>
 * Contrôleur lié à la vue main, il permet de choisir entre ouvrir une sauvegarde pré-existante,
 * une sauvegarde interne a l'application ou créer une nouvelle sauvegarde en commençant le paramétrage.
 *
 * @author Timothé CABON
 * @version 1.0.0
 */
public class MainController implements Initializable {

    @FXML
    private Label errorLabel;
    @FXML
    private RadioButton radioSave;
    @FXML
    private RadioButton radioNewFile;
    @FXML
    private TextField url;
    @FXML
    private ImageView logo;

    /**
     * Méthode d'initialisation du contrôleur.
     * Désactive ou active le <code>RadioButton radioInternal</code> en fonction de la présence d'une sauvegarde interne ou non
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     *                  the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        App.setStageSize(500, 350, 500, 350);
        Image img = new Image(App.class.getResource("image/pH_logo.png").toString());
        logo.setImage(img);
    }

    /**
     * Méthode error'exécutant lors d'un clic sur le <code>TextField url</code>.
     * Ouvre un explorateur de fichiers qui autorise seulement l'entrée d'un fichier .db.
     */
    @FXML
    private void openFileExplorer() {
        FileChooser fileChooser = new FileChooser();
        if (!url.getText().equals("")) {
            File file = new File(url.getText());
            if (file.getParentFile() != null) {
                fileChooser.setInitialDirectory(file.getParentFile());
            }
        }
        FileChooser.ExtensionFilter extFiler = new FileChooser.ExtensionFilter("pH save (*.pHs)", "*.pHs");
        fileChooser.getExtensionFilters().add(extFiler);
        try {
            File selectedDir = fileChooser.showOpenDialog(url.getScene().getWindow());
            if (selectedDir != null) {
                url.setText(selectedDir.getPath());
            }
        } catch (NullPointerException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Le fichier que vous avez sélectionné n'existe plus ou est inaccessible.");
            alert.showAndWait();
        }
    }


    /**
     * Méthode s'exécutant lors d'un clic sur le bouton "suivant".
     * Change de vue en fonction du bouton radio sélectionné.
     *
     * @throws IOException S'il y à un problème relatif à l'ouverture de la vue.
     */
    @FXML
    private void next() throws IOException {
        if (radioSave.isSelected()) {
            if (url.getText().trim().isEmpty()) {
                setTextErrorLabel("Veuillez sélectionner un fichier pour continuer.");
                return;
            }

            if (!Files.exists(Path.of(url.getText()))) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
                alert.setContentText("Le fichier que vous avez sélectionné n'existe plus ou est inaccessible.");
                alert.showAndWait();
            }

            Connection connection = SqliteConnexion.getInstance(url.getText()).getConnexion();
            Database database = Database.getDatabase(connection);
            App.setApplicants(database.restore());
            //TODO: restore base questions

            FXMLLoader loader = App.getLoader("application");
            Parent x = loader.load();
            App.setRoot(x);
        } else if (radioNewFile.isSelected()) {
            App.setRoot(App.getLoader("openExcel").load());
        }
    }

    /**
     * Méthode permettant de changer l'état du <code>RadioButton radioURL</code>.
     *
     * @param state Vrai si le bouton radio de l'URL doit être sélectionnée, faux sinon.
     */
    public void setRadioSave(boolean state) {
        radioSave.setSelected(state);
    }

    /**
     * Méthode permettant de changer l'état du <code>RadioButton radioNewFile</code>.
     *
     * @param state Vrai si le bouton radio du nouveau fichier doit être sélectionnée, faux sinon.
     */
    public void setRadioNewFile(boolean state) {
        radioNewFile.setSelected(state);
    }

    /**
     * Méthode permettant d'afficher un message d'erreur en bas a gauche de la vue.
     *
     * @param error Le message d'erreur à afficher en bas de la vue.
     */
    public void setTextErrorLabel(String error) {
        errorLabel.setText(error);
    }
}
