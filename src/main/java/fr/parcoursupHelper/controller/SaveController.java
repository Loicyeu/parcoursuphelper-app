package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.SqliteConnexion;
import fr.parcoursupHelper.model.database.Database;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

public class SaveController implements Initializable {

    @FXML
    private Button browse;
    @FXML
    private Label fileName;
    @FXML
    private Button cancel;
    @FXML
    private Button apply;

    private File saveFile;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        apply.setDisable(true);

        browse.setOnMouseClicked(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Sauvegarder");
            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("pH save (*.pHs)", "*.pHs");
            fileChooser.getExtensionFilters().add(extensionFilter);
            File file = fileChooser.showSaveDialog(browse.getScene().getWindow());
            if (file != null) {
                saveFile = file;
                fileName.setText(file.getName());
                apply.setDisable(false);
            }
        });

        cancel.setOnMouseClicked(event -> close());

        apply.setOnMouseClicked(event -> {
            try {
                SqliteConnexion connexion = SqliteConnexion.getExternalInstance(saveFile.toURI().toURL());
                Database database = Database.getDatabase(connexion.getConnexion());
                database.saveAll(App.getApplicants().toList());
                close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        });

    }

    private void close() {
        ((Stage) cancel.getScene().getWindow()).close();
    }
}
