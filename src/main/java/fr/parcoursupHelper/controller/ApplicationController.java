package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.applicant.Applicant;
import fr.parcoursupHelper.model.applicant.Applicants;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ApplicationController implements Initializable {


    @FXML
    private ListView<Applicant> viewListApplicants;
    @FXML
    private VBox container;
    @FXML
    private Button previousApplicant;
    @FXML
    private Button nextApplicant;

    private Applicants applicants;

    /**
     * Méthode d'initialisation du contrôleur ApplicationController
     *
     * @param url            url
     * @param resourceBundle resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        applicants = App.getApplicants();

        viewListApplicants.setItems(applicants.toObservableList());
        viewListApplicants.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            container.getChildren().clear();
            if (newValue != null) {
                container.getChildren().add(newValue.getDisplayable());
            }
            updateApplicantState();
            viewListApplicants.refresh();
            previousApplicant.setDisable(viewListApplicants.getSelectionModel().getSelectedIndex() <= 0);
        });

        previousApplicant.setDisable(true);
        previousApplicant.setOnMouseClicked(event -> {
            SelectionModel<Applicant> selectionModel = viewListApplicants.getSelectionModel();
            selectionModel.select(selectionModel.getSelectedIndex() - 1);
            previousApplicant.setDisable(selectionModel.getSelectedIndex() <= 0);
            nextApplicant.setDisable(selectionModel.getSelectedIndex() >= applicants.getSize() - 1);
        });

        nextApplicant.setOnMouseClicked(event -> {
            SelectionModel<Applicant> selectionModel = viewListApplicants.getSelectionModel();
            selectionModel.select(selectionModel.getSelectedIndex() + 1);
            previousApplicant.setDisable(selectionModel.getSelectedIndex() <= 0);
            nextApplicant.setDisable(selectionModel.getSelectedIndex() >= applicants.getSize() - 1);
        });
    }

    /**
     * Méthode s'exécutant lors d'un clic sur la <code>VBox container</code> ou sur la <code>ListView<Applicant> viewListApplicants</code>.
     * Met à jour le state de chaque Applicant.
     */
    @FXML
    private void updateApplicantState() {
        for (Applicant applicant : applicants) {
            applicant.updateState();
        }
        viewListApplicants.refresh();
    }

    /**
     * Lorsque l'utilisateur souhaite sauvegarder son activité.
     * La sauvegarde doit être interne.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    public void save(Event event) {
        App.openSave();
    }

    /**
     * Lorsque l'utilisateur souhaite quitter l'application.<br>
     * <b>DOIT</b> demander si l'utilisateur souhaite sauvegarder son activité avant de quitter.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    public void exit(Event event) {
        
    }

    /**
     * Lorsque l'utilisateur souhaite avoir les crédits de l'application
     *
     * @param event L'évènement JFX.
     */
    @FXML
    public void aboutUs(Event event) {
        App.aboutUs();
    }

    /**
     * Lorsque l'utilisateur requière l'aide en ligne.
     *
     * @param event L'évènement JFX.
     */
    @FXML
    public void onlineHelp(Event event) {
        App.openWebsite();
    }

    @FXML
    public void export() {
        FXMLLoader loader = App.getLoader("exportResults");
        try {
            Parent root = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Exporter les résultats");
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(container.getScene().getWindow());
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
