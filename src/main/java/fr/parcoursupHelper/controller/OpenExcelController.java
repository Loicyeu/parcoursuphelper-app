package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.applicant.Applicant;
import fr.parcoursupHelper.model.applicant.Applicants;
import fr.parcoursupHelper.model.excel.ExcelException;
import fr.parcoursupHelper.model.excel.ExcelReader;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Classe OpenExcelController.<br>
 * Contrôleur lié à la vue openExcel. Permet de charger le fichier excel contenant tous les numéros des candidats,
 * de les instancier et de les stocker.
 *
 * @author Timothé CABON
 * @version 1.0.0
 */
public class OpenExcelController implements Initializable {

    @FXML
    private Label errorLabel;
    @FXML
    private Label browseLabel;
    @FXML
    private TextField columnTextField;

    private File excelFile;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     *                  the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        App.setStageSize(500, 300, 500, 300);
    }

    /**
     * Méthode error'exécutant lors d'un clic sur le bouton "Précédant".<br>
     * Retourne sur la vue précédente : "main"
     *
     * @throws IOException S'il y a une erreur relative au changement de vue.
     */
    @FXML
    private void previous() throws IOException {
        FXMLLoader loader = App.getLoader("main");
        Parent x = loader.load();
        try {
            MainController controller = loader.getController();
            controller.setRadioNewFile(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        App.setRoot(x);
    }

    /**
     * Méthode error'exécutant lors d'un clic sur le bouton "Suivant".<br>
     * Permet de passer à la vue suivante : "createQuestion" si le fichier excel est correct.
     * Dans le cas ou le fichier ne serait pas correcte, affiche un message d'erreur en bas de la vue.
     */
    @FXML
    private void next() {
        if (excelFile == null) {
            setTextErrorLabel("Veuillez sélectionné un fichier excel pour continuer");
            return;
        }
        if (columnTextField.getText().trim().isEmpty()) {
            setTextErrorLabel("Veuillez choisir un nom de colonne");
            return;
        }
        List<Integer> idList;
        try {
            idList = ExcelReader.getIDsFromColumn(excelFile, columnTextField.getText());
        } catch (ExcelException | IllegalArgumentException | FileNotFoundException e) {
            setTextErrorLabel(e.getMessage());
            return;
        }

        try {
            List<Applicant> applicantList = new ArrayList<>();
            idList.forEach(integer -> applicantList.add(new Applicant(integer)));
            App.setApplicants(new Applicants(applicantList));
            FXMLLoader loader = App.getLoader("createQuestions");
            Parent x = loader.load();
            App.setRoot(x);
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
            setTextErrorLabel("Veuillez verifier que la colonne saisie est correcte.");
        }
    }

    /**
     * Méthode permettant d'afficher un message d'erreur en bas a gauche de la vue.
     *
     * @param error Le message d'erreur à afficher.
     */
    public void setTextErrorLabel(String error) {
        errorLabel.setText(error);
    }

    /**
     * Méthode error s'exécutant lors d'un clic sur le bouton "parcourir".<br>
     * Ouvre un explorateur de fichiers qui autorise seulement l'entrée d'un fichier excel (.xls, .xlsx, .xlm),
     * permettant ainsi de récupérer le chemin vers le fichier sélectionné par l'utilisateur.
     */
    @FXML
    private void browse(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        if (excelFile != null && excelFile.getParentFile() != null) {
            fileChooser.setInitialDirectory(excelFile.getParentFile());
        }
        FileChooser.ExtensionFilter extFiler = new FileChooser.ExtensionFilter("Excel files (*.xls, *.xlsx)", "*.xls", "*.xlsx");
        fileChooser.getExtensionFilters().add(extFiler);

        File file = fileChooser.showOpenDialog(browseLabel.getScene().getWindow());
        if (file != null) {
            excelFile = file;
            browseLabel.setText(excelFile.getName());
        }
    }
}
