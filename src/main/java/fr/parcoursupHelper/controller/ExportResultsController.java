package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.excel.ExcelReader;
import fr.parcoursupHelper.model.question.Question;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

public class ExportResultsController implements Initializable {
    @FXML
    private VBox checkboxesVBox;
    @FXML
    private CheckBox includeGlobalMark;
    @FXML
    private HBox globalMarkDetails;
    @FXML
    private TextField globalMarkMaxValue;
    @FXML
    private Label errorLabel;

    private Set<Question> selectedQuestions;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectedQuestions = new HashSet<>();
        includeGlobalMark.setOnAction(event -> globalMarkDetails.setDisable(!includeGlobalMark.isSelected()));

        checkboxesVBox.getChildren().clear();
        for (Question question : App.getQuestions()) {
            CheckBox checkBox = new CheckBox(question.getName());
            checkBox.setSelected(true);
            checkBox.setPadding(new Insets(0, 0, 10, 0));
            checkBox.setOnAction(event -> {
                if (checkBox.isSelected()) {
                    selectedQuestions.add(question);
                } else {
                    selectedQuestions.remove(question);
                }
            });
            selectedQuestions.add(question);
            checkboxesVBox.getChildren().add(checkBox);
        }

    }

    @FXML
    private void cancel(ActionEvent actionEvent) {
        ((Stage) checkboxesVBox.getScene().getWindow()).close();
    }

    @FXML
    private void export(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Excel file", "*.xlsx", "*.xls");
        fileChooser.getExtensionFilters().add(extensionFilter);
        fileChooser.setTitle("Exporter");
        File file = fileChooser.showSaveDialog(checkboxesVBox.getScene().getWindow());
        if (file != null) {
            try {
                if (file.getName().endsWith(".xlsx")) {
                    ExcelReader.exportXLSX(file, App.getApplicants(), Integer.parseInt(globalMarkMaxValue.getText()));
                } else {
                    ExcelReader.exportXLS(file, App.getApplicants(), Integer.parseInt(globalMarkMaxValue.getText()));
                }
                ((Stage) checkboxesVBox.getScene().getWindow()).close();
            } catch (NumberFormatException e) {
                errorLabel.setText("La valeur maximal de la note globale n'est pas valide.");
            }
        }
    }
}
