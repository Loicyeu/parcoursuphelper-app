package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class PopUpRenameMarkSystemController implements Initializable {

    PopUpRenameMarkSystemController controller;
    @FXML
    TextField rename;
    @FXML
    Button confirm;
    private int idmodified;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        confirm.setOnAction(event -> confirmRename());
    }

    public void setController(PopUpRenameMarkSystemController controller) {
        this.controller = controller;
    }

    public void confirmRename() {
        String string = rename.getText();
        App.getMarkRegistry().getMarkSystems().get(idmodified).setName(string);
        ((Stage) rename.getScene().getWindow()).close();
    }

    public void setIdModified(int number) {
        this.idmodified = number;
    }
}
