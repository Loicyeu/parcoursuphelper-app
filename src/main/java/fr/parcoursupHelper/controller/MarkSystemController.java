package fr.parcoursupHelper.controller;

import fr.parcoursupHelper.App;
import fr.parcoursupHelper.model.mark.Mark;
import fr.parcoursupHelper.model.mark.MarkSystem;
import fr.parcoursupHelper.model.mark.MarkSystemRegistry;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.*;

public class MarkSystemController implements Initializable {

    @FXML
    private Button applyButton;
    @FXML
    public ScrollPane displaySystems;

    @FXML
    private Button addSystem;
    @FXML
    private Button removeSystem;

    @FXML
    private ListView<MarkSystem> notationSystemListInterface;

    private MarkSystemRegistry systemRegistry = new MarkSystemRegistry();
    private MarkSystemController controller;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     *                  the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (!App.getMarkRegistry().getMarkSystems().isEmpty()) {
            populateMarkList();
            System.out.println("POPULATED");
        }
        System.out.println("AHAH");
        System.out.println(systemRegistry.getMarkSystems());

        notationSystemListInterface.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        notationSystemListInterface.getSelectionModel().selectedItemProperty().addListener(
                (ov, old_val, new_val) -> {
                    if (new_val != null) {
                        Button button = new Button("Ajouter notation");
                        button.setOnAction(event -> {
                            new_val.getMarkList().add(new Mark("", ""));
                            displaySystems.setContent(new_val.getCreatingDisplayable(button));
                        });
                        displaySystems.setContent(new_val.getCreatingDisplayable(button));
                    } else {
                        displaySystems.setContent(null);
                    }
                });

        notationSystemListInterface.setOnMouseClicked(mouseEvent -> {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                if(mouseEvent.getClickCount() == 2){
                    ListView<MarkSystem> lv = (ListView<MarkSystem>)mouseEvent.getSource();
                    FXMLLoader loader = App.getLoader("popupChangeMarkSystemName");
                    try {
                        Parent root = loader.load();
                        PopUpRenameMarkSystemController controller = loader.getController();
                        controller.setController(new PopUpRenameMarkSystemController());
                        controller.setIdModified(lv.getSelectionModel().getSelectedIndex());
                        Stage stage = new Stage();
                        stage.setScene(new Scene(root));
                        stage.setTitle("Renommer le système de notation");
                        stage.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    private void populateMarkList() {
        this.systemRegistry = App.getMarkRegistry();
        this.notationSystemListInterface.getItems().setAll(systemRegistry.getMarkSystems());
    }

    @FXML
    private void close() {
        App.setMarkSystems(systemRegistry);
        System.out.println(App.getMarkRegistry().getMarkSystems());
        ((Stage) applyButton.getScene().getWindow()).close();
    }

    public void setController(MarkSystemController controller) {
        this.controller = controller;
    }

    public void addSystem() {
        MarkSystem markSystem = new MarkSystem("Système de notation");
        systemRegistry.addMarkSystem(markSystem);
        notationSystemListInterface.getItems().add(markSystem);
    }


    public void deleteSystem() {
        MarkSystem markSystem = notationSystemListInterface.getSelectionModel().getSelectedItem();
        systemRegistry.removeMarkSystem(markSystem);
        notationSystemListInterface.getItems().remove(markSystem);
        notationSystemListInterface.refresh();
    }
}
