package fr.parcoursupHelper.model.database;

import fr.loicyeu.dao.Dao;
import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.applicant.Applicant;
import fr.parcoursupHelper.model.applicant.Applicants;
import fr.parcoursupHelper.model.question.MultipleQuestion;
import fr.parcoursupHelper.model.question.Question;
import fr.parcoursupHelper.model.question.SliderQuestion;
import fr.parcoursupHelper.model.question.UniqueQuestion;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public final class Database {

    private static Database database;

    private final Dao<Applicant> applicantDao;

    private final Dao<SliderQuestion> sliderQuestionDao;
    private final Dao<UniqueQuestion> uniqueQuestionDao;
    private final Dao<MultipleQuestion> multipleQuestionDao;
    private final Dao<Answer> answerDao;

    private Database(Connection connection) {

        this.applicantDao = new Dao<>(connection, Applicant.class);

        this.sliderQuestionDao = new Dao<>(connection, SliderQuestion.class);
        this.uniqueQuestionDao = new Dao<>(connection, UniqueQuestion.class);
        this.multipleQuestionDao = new Dao<>(connection, MultipleQuestion.class);
        this.answerDao = new Dao<>(connection, Answer.class);

        this.applicantDao.hasMany(sliderQuestionDao, "applicantSlider");
        this.applicantDao.hasMany(uniqueQuestionDao, "applicantUnique");
        this.applicantDao.hasMany(multipleQuestionDao, "applicantMultiple");
        this.multipleQuestionDao.hasMany(answerDao, "multipleAnswer");
        this.uniqueQuestionDao.hasMany(answerDao, "uniqueAnswer");

        this.applicantDao.createTable(false);
        this.answerDao.createTable(false);
        this.sliderQuestionDao.createTable(false);
        this.uniqueQuestionDao.createTable(false);
        this.multipleQuestionDao.createTable(false);
    }

    public static Database getDatabase(Connection connection) {
        if(database==null) {
            database = new Database(connection);
        }
        return database;
    }

    public void saveAll(List<Applicant> applicants) {
        this.applicantDao.purge();
        this.sliderQuestionDao.purge();
        this.uniqueQuestionDao.purge();
        this.multipleQuestionDao.purge();

        for (Applicant applicant : applicants) {
            applicantDao.insert(applicant);
            for (Question question : applicant.getQuestions()) {
                if (question instanceof SliderQuestion) {
                    SliderQuestion sliderQuestion = ((SliderQuestion) question);
                    sliderQuestionDao.insert(sliderQuestion);
                    applicantDao.insert1NRelation(sliderQuestionDao, applicant, sliderQuestion);
                } else if (question instanceof MultipleQuestion) {
                    MultipleQuestion multipleQuestion = ((MultipleQuestion) question);
                    multipleQuestionDao.insert(multipleQuestion);
                    applicantDao.insert1NRelation(multipleQuestionDao, applicant, multipleQuestion);
                    for (Answer answer : multipleQuestion.getAnswers()) {
                        answerDao.insert(answer);
                        multipleQuestionDao.insert1NRelation(answerDao, multipleQuestion, answer);
                    }
                } else if (question instanceof UniqueQuestion) {
                    UniqueQuestion uniqueQuestion = ((UniqueQuestion) question);
                    uniqueQuestionDao.insert(uniqueQuestion);
                    applicantDao.insert1NRelation(uniqueQuestionDao, applicant, uniqueQuestion);
                    for (Answer answer : uniqueQuestion.getAnswers()) {
                        answerDao.insert(answer);
                        uniqueQuestionDao.insert1NRelation(answerDao, uniqueQuestion, answer);
                    }
                }
            }
        }
    }

    public Applicants restore() {
        List<Applicant> applicants = applicantDao.findAll();

        for (Applicant applicant : applicants) {
            List<Question> questions = new ArrayList<>(applicantDao.findFrom1NRelation(sliderQuestionDao, applicant));

            List<MultipleQuestion> multipleQuestions = applicantDao.findFrom1NRelation(multipleQuestionDao, applicant);
            for (MultipleQuestion answersQuestion : multipleQuestions) {
                answersQuestion.addAnswers(multipleQuestionDao.findFrom1NRelation(answerDao, answersQuestion));
            }
            questions.addAll(multipleQuestions);

            List<UniqueQuestion> uniqueQuestions = applicantDao.findFrom1NRelation(uniqueQuestionDao, applicant);
            for (UniqueQuestion uniqueQuestion : uniqueQuestions) {
                uniqueQuestion.addAnswers(uniqueQuestionDao.findFrom1NRelation(answerDao, uniqueQuestion));
            }
            questions.addAll(uniqueQuestions);

            questions.addAll(applicantDao.findFrom1NRelation(uniqueQuestionDao, applicant));
            applicant.addQuestions(questions);
        }
        return new Applicants(applicants);
    }

}
