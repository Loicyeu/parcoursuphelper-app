package fr.parcoursupHelper.model.excel;

import fr.parcoursupHelper.model.applicant.Applicant;
import fr.parcoursupHelper.model.applicant.Applicants;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe ExcelReader.<br>
 * Permet de lire ou écrire un fichier Excel de type XLS ou XLSX.
 *
 * @author Timothé CABON, Loïc HENRY
 * @version 1.2.0
 */
public class ExcelReader {

    /**
     * Constructeur privé permettant de ne pas créer d'instance de cette classe statique.
     */
    private ExcelReader() {

    }

    /**
     * Méthode permettant de récupérer une liste de numéro dans une colonne.
     * Si un titre est donnée a la colonne celui-ci sera ignorer.
     *
     * @param file   Le fichier excel a lire (XLS ou XLSX).
     * @param column Le numéro de la colonne (numérique ou alphabétique).
     * @return La liste des numéros dans la colonne donnée.
     * @throws FileNotFoundException    Si le fichier n'a pas été trouvé.
     * @throws IllegalArgumentException Si l'extension du fichier n'est pas valide.
     * @throws ExcelException           Si le numéro de la colonne n'est pas valide ou si une autre erreur de excel survient.
     */
    public static List<Integer> getIDsFromColumn(File file, String column) throws FileNotFoundException, IllegalArgumentException, ExcelException {

        int colNum = 0;
        if (column.matches("^[0-9]+$")) {
            try {
                colNum = Integer.parseInt(column);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("La colonne (" + column + ") n'est pas valide.");
            }
        } else if (column.matches("^[a-zA-Z]+$")) {
            char[] chars = new char[column.length()];
            column.toUpperCase().getChars(0, column.length(), chars, 0);

            for (int i = 0; i < chars.length; i++) {
                colNum += (chars[i] - '@') * Math.pow(26, chars.length - i - 1);
            }
            colNum--;
        } else {
            throw new IllegalArgumentException("La colonne (" + column + ") n'est pas valide.");
        }

        String[] fileName = file.getName().split("\\.");
        List<Integer> listID = new ArrayList<>();
        Workbook workbook;

        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            switch (fileName[fileName.length - 1]) {
                case "xlsx" -> workbook = new XSSFWorkbook(fileInputStream);
                case "xls" -> workbook = new HSSFWorkbook(fileInputStream);
                default -> throw new IllegalArgumentException("L'extension du fichier n'est pas valide (" + fileName[fileName.length - 1] + ").");
            }

            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                Cell cell = row.getCell(colNum);

                CellType cellType = cell.getCellType();
                if (cellType == CellType.NUMERIC) {
                    listID.add((int) cell.getNumericCellValue());
                } else if (cellType == CellType.STRING) {
                    try {
                        listID.add(Integer.parseInt(cell.getStringCellValue()));
                    } catch (NumberFormatException ignored) {
                        System.out.println("La valeur ('" + cell.getStringCellValue() + "') trouvée dans la colonne n'a pas pu être interprété comme un numéro.");
                    }
                }
            }
            workbook.close();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Le fichier '" + file.getName() + "' n'a pas pu être trouvé.");
        } catch (IOException e) {
            throw new ExcelException(e);
        }
        if (listID.isEmpty()) {
            throw new ExcelException("Aucun numéro de candidats n'ont été trouvés dans la colonne '" + column + "'.");
        }
        return listID;
    }

    /**
     * Méthode permettant d'exporter les données dans un fichier Excel XLS.
     *
     * @param file       Le fichier de destination des données en <code>.xls</code>.
     * @param applicants La liste des candidats à exporter.
     * @param markOn     La note maximal sur laquelle doivent être calculés les moyennes.
     */
    public static void exportXLS(File file, Applicants applicants, int markOn) {
        export(new HSSFWorkbook(), file, applicants, markOn);
    }

    /**
     * Permet d'exporter les données dans un fichier Excel XLSX.
     *
     * @param file       Le fichier de destination des données en <code>.xlsx</code>.
     * @param applicants La liste des candidats à exporter.
     * @param markOn     La note maximal sur laquelle doivent être calculés les moyennes.
     */
    public static void exportXLSX(File file, Applicants applicants, int markOn) {
        export(new XSSFWorkbook(), file, applicants, markOn);
    }

    private static void export(Workbook workbook, File file, Applicants applicants, int markOn) {
        try {
            Sheet sheet = workbook.createSheet("Notes globales");

            Row rowhead = sheet.createRow(0);
            rowhead.createCell(0).setCellValue("Numéro du candidat");
            rowhead.createCell(1).setCellValue("Note globale (/20)");
            rowhead.createCell(2).setCellValue("Commentaire");

            for (int i = 1; i <= applicants.getSize(); i++) {
                Row row = sheet.createRow(i);
                Applicant applicant = applicants.get(i - 1);
                row.createCell(0).setCellValue(applicant.getId());
                row.createCell(1).setCellValue(applicant.getMark(markOn));
                row.createCell(2).setCellValue(applicant.getComment());
            }

            FileOutputStream fileOut = new FileOutputStream(file);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
