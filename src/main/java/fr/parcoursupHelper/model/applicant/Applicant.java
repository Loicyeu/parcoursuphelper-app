package fr.parcoursupHelper.model.applicant;

import fr.loicyeu.dao.FieldType;
import fr.loicyeu.dao.annotations.DaoField;
import fr.loicyeu.dao.annotations.DaoTable;
import fr.loicyeu.dao.annotations.PrimaryKey;
import fr.parcoursupHelper.model.question.Question;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Applicant.<br>
 * Représente un candidat sur l'application.
 *
 * @author Timothé CABON
 * @version 1.1.0
 */
@DaoTable(tableName = "applicant")
public class Applicant {

    @DaoField(name = "id", type = FieldType.INT)
    @PrimaryKey
    private final int id;

    private final List<Question> questions;

    @DaoField(name = "comment", type = FieldType.LONG_VARCHAR)
    private String comment;

    private State state;

    public Applicant() {
        this(0);
    }

    /**
     * Constructeur de la classe Applicant.
     *
     * @param id l'id de l'Applicant.
     */
    public Applicant(int id) {
        this(id, "", State.UNTREATED);
    }


    public Applicant(int id, String comment, State state) {
        this.id = id;
        this.comment = comment;
        this.state = state;
        questions = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    /**
     * Méthode permettant de récupérer la note globale d'un candidat.
     *
     * @return La note globale d'une candidat.
     * @throws IllegalStateException Si le candidat n'a pas de question ou si son état n'est pas a : <code>State.DONE</code>.
     */
    public float getMark(int markOn) {
        if (questions == null || questions.isEmpty())
            throw new IllegalStateException("Le candidat n'a pas questions.");
        if (state != State.DONE)
            throw new IllegalStateException("La notation du candidat n'est pas terminé.");

        float marks = 0f;
        float weightings = 0f;

        int i = 0;
        for (Question question : this.questions) {
            System.out.println("Question n°" + i + " : Note = " + question.getMark() + " , Weighting = " + question.getWeighting());
            marks += question.getMark() * question.getWeighting();
            weightings += question.getWeighting();
            i++;
        }

        return (marks / weightings) * markOn;
    }

    public String toString() {
        return state.getIcon() + " - " + id;
    }

    /**
     * Méthode ajoutant une question dans la liste de question du candidat.
     *
     * @param question la question à ajouter.
     */
    public void addQuestion(Question question) {
        question.setUid("APPLICANT-" + id);
        questions.add(question);
    }

    /**
     * Méthode ajoutant toutes les questions passés en paramètre dans la liste de question du candidat.
     *
     * @param questions La liste des questions à ajouter.
     */
    public void addQuestions(List<Question> questions) {
        questions.forEach(this::addQuestion);
    }

    /**
     * Getter de l'attribut questions
     *
     * @return questions
     */
    public List<Question> getQuestions() {
        return questions;
    }

    /**
     * Méthode permettant de récupérer une VBox contenant les questions et réponses de l'Applicant.
     *
     * @return une VBox contenant les questions et réponses propre à l'Applicant.
     */
    public VBox getDisplayable() {
        VBox vbox = new VBox();
        for (Question question : questions) {
            VBox questionVBox = question.getDisplayable();
            VBox.setMargin(questionVBox, new Insets(0, 0, 10, 0));
            vbox.getChildren().add(questionVBox);
        }
        vbox.getChildren().add(new Separator());
        Label comm = new Label("Commentaire global du candidat : ");
        comm.setFont(Font.font(null, FontWeight.BOLD, -1));
        vbox.getChildren().add(comm);
        TextArea textArea = new TextArea(comment);
        textArea.setPrefColumnCount(25);
        textArea.setWrapText(true);
        textArea.setOnKeyPressed(event -> comment = textArea.getText());
        vbox.getChildren().add(textArea);
        return vbox;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void updateState() {
        int cpt = 0;
        for (Question question : questions) {
            question.updateState();
            if (question.getState() == Question.State.DONE) {
                cpt++;
            }
            if (cpt == questions.size()) {
                state = State.DONE;
                return;
            }
        }
        if (cpt == 0) {
            state = State.UNTREATED;
        } else {
            state = State.IN_PROGRESS;
        }
    }

    public enum State {
        UNTREATED(0, "\uD83D\uDD34 U"),
        IN_PROGRESS(1, "\uD83D\uDFE1 I"),
        DONE(2, "\uD83D\uDFE2 D");

        private final int stateNumber;
        private final String icon;

        State(int state, String icon) {
            this.stateNumber = state;
            this.icon = icon;
        }

        /**
         * Méthode permettant de récupérer un état par rapport a un numéro d'état.
         *
         * @param stateNumber Le numéro de l'état a récupérer.
         * @return L'état correspondant au numéro.
         * @throws IllegalArgumentException si le numéro ne correspond a aucun état.
         */
        public static State getState(int stateNumber) {
            return switch (stateNumber) {
                case 0 -> UNTREATED;
                case 1 -> IN_PROGRESS;
                case 2 -> DONE;
                default -> throw new IllegalArgumentException("Aucun State ne correspond a " + stateNumber);
            };
        }

        /**
         * Méthode permettant de récupérer le numéro associé a l'état.
         *
         * @return Le numéro associé a l'état.
         */
        public int getStateNumber() {
            return stateNumber;
        }

        /**
         * Méthode permettant de récupérer l'icône associé a l'état.
         * Un rond vert pour <code>DONE</code>, jaune pour <code>IN_PROGRESS</code>, et rouge pour <code>UNTREATED</code>.
         *
         * @return l'icône associé a l'état.
         */
        public String getIcon() {
            return icon;
        }
    }


}
