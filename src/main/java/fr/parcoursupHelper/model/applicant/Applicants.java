package fr.parcoursupHelper.model.applicant;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Iterator;
import java.util.List;

public class Applicants implements Iterable<Applicant> {

    private final List<Applicant> applicantList;

    /**
     * Constructeur permettant de créer l'objet Applicants à partir d'une liste de candidat.
     *
     * @param applicantList La liste de candidat.
     */
    public Applicants(List<Applicant> applicantList) {
        this.applicantList = applicantList;
    }

    /**
     * Méthode permettant de récupérer un candidat à un index précis.
     *
     * @param index L'index du candidat dans la liste.
     * @return Le candidat à l'index souhaité.
     */
    public Applicant get(int index) {
        return applicantList.get(index);
    }

    /**
     * Méthode retournant le nombre de candidats présents dans la liste.
     *
     * @return Le nombre de candidats dans la liste.
     */
    public int getSize() {
        return applicantList.size();
    }

    /**
     * Méthode permettant de récupérer la liste des candidats.
     *
     * @return La liste des candidats.
     */
    public List<Applicant> toList() {
        return applicantList;
    }

    /**
     * Méthode permettant d'obtenir une ObservableList de tous les candidats.
     *
     * @return Une ObservableList des candidats.
     */
    public ObservableList<Applicant> toObservableList() {
        return FXCollections.observableArrayList(applicantList);
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Applicant> iterator() {
        return applicantList.iterator();
    }

    @Override
    public String toString() {
        return "Applicants{" +
                "applicants=" + applicantList +
                '}';
    }
}