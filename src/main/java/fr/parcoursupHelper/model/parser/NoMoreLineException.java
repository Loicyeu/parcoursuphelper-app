package fr.parcoursupHelper.model.parser;

/**
 * Exception NoMoreLineException.
 * Permet de signifier que le fichier de question/réponse n'est pas valide.
 *
 * @version 1.0.0
 * @author Loïc HENRY
 */
public class NoMoreLineException extends RuntimeException{
    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    NoMoreLineException() {
    }

    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    NoMoreLineException(String message) {
        super(message);
    }
}