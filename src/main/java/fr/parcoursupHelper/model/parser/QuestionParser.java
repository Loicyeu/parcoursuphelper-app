package fr.parcoursupHelper.model.parser;

import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.question.AnswersQuestion;
import fr.parcoursupHelper.model.question.Question;
import fr.parcoursupHelper.model.question.annotation.QuestionParseField;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.FileAlreadyExistsException;
import java.util.Collection;

/**
 * Classe QuestionParser.
 * Permet de parser toutes les questions dans un ficher de questions.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
public final class QuestionParser {

    private final Writer writer;

    /**
     * Constructeur de la classe FileParser.<br>
     *
     * @param file Le fichier dans lequel parser les questions.
     * @throws FileAlreadyExistsException Si le fichier existe déjà.
     */
    public QuestionParser(File file) throws IOException {
        this.writer = new Writer(file);
    }

    public int parse(Collection<Question> questions) {
        int nb = questions.size();
        for (Question question : questions) {
            try {
                parseQuestion(question);
                writer.flush();
            }catch (IOException e) {
                e.printStackTrace();
                writer.emptyBuffer();
                nb--;
            }
        }
        return nb;
    }

    private void parseQuestion(Question question) {
        writer.appendLine("Question :");
        writer.appendLine("\tname: "+question.getName());
        writer.appendLine("\tlabel: "+question.getLabel());
        writer.appendLine("\tweighting: "+question.getWeighting());

        Field[] fields = question.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            QuestionParseField questionField = field.getAnnotation(QuestionParseField.class);
            if (questionField != null) {
                try {
                    writer.appendLine("\t"+questionField.name()+": "+field.get(question));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        if(question instanceof AnswersQuestion) {
            parseAnswers((AnswersQuestion) question);
        }
        writer.appendLine("\n");
    }

    private void parseAnswers(AnswersQuestion question) {
        for (Answer answer : question.getAnswers()) {
            writer.appendLine("\tanswer:");
            writer.appendLine("\t\tlabel:"+answer.getLabel());
            writer.appendLine("\t\tvalue:"+answer.getValue());
        }
    }
}
