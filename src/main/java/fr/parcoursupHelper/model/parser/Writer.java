package fr.parcoursupHelper.model.parser;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Writer implements Closeable {

    private final FileWriter fileWriter;
    private final List<String> buffer;

    public Writer(File file) throws IOException {
        this.fileWriter = new FileWriter(file);
        this.buffer = new ArrayList<>();
    }

    /**
     * Méthode permettant d'ajouter une ligne vide dans le buffer.
     */
    public void appendLine() {
        buffer.add("\n");
    }

    /**
     * Méthode permettant d'ajouter une ligne dans le buffer. Si la ligne donné ne finit pas par '\n'
     * alors l'ajoute à la fin.
     *
     * @param line La ligne à ajouter dans le buffer.
     */
    public void appendLine(String line) {
        buffer.add(line.endsWith("\n")?line:line+"\n");
    }

    /**
     * Méthode permettant d'ajouter du texte dans le buffer.
     * @param text Le texte à ajouter dans le buffer.
     */
    public void append(String text) {
        buffer.add(text);
    }

    /**
     * Méthode permettant de transférer le contenu du buffer dans le fichier de question.
     * @throws IOException Si une erreur I/O survient.
     */
    public void flush() throws IOException {
        for (String line : buffer) {
            fileWriter.write(line);
        }
        fileWriter.flush();
    }

    /**
     * Méthode permettant de vider le buffer.
     */
    public void emptyBuffer() {
        buffer.clear();
    }

    /**
     * Closes this stream and releases any system resources associated
     * with it. If the stream is already closed then invoking this
     * method has no effect.
     *
     * <p> As noted in {@link AutoCloseable#close()}, cases where the
     * close may fail require careful attention. It is strongly advised
     * to relinquish the underlying resources and to internally
     * <em>mark</em> the {@code Closeable} as closed, prior to throwing
     * the {@code IOException}.
     *
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void close() throws IOException {
        fileWriter.close();
    }
}