package fr.parcoursupHelper.model.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Classe Reader<br>
 * Permet de lire un fichier de question passé en paramètre. <br>
 * Ne lis pas les commentaire (lignes commençant par "{@code //}").
 *
 * @author Loïc HENRY
 * @version 1.1.0
 * @see Scanner
 */
class Reader {
    private final List<String> lineList;
    private int pos;

    /**
     * Constructeur de la classe Reader.
     *
     * @param file Le fichier a lire.
     * @throws FileNotFoundException Si le fichier n'existe pas.
     */
    public Reader(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        this.lineList = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.trim().startsWith("//")) {
                lineList.add(line);
            }
        }
        scanner.close();
        this.pos = -1;
    }

    /**
     * Méthode permettant de récupérer la ligne précédente, sans modifier la position du curseur.
     *
     * @return La ligne précédente.
     * @throws NoMoreLineException S'il n'y a pas de ligne précédente.
     */
    public String previousLine() throws NoMoreLineException {
        if (pos <= 0)
            throw new NoMoreLineException("Pas de ligne précédent la N°" + pos);

        return lineList.get(pos - 1);
    }

    /**
     * Méthode permettant de récupérer la nouvelle ligne dans le fichier et d'avancer la position du curseur.
     *
     * @return La ligne suivante dans le fichier.
     * @throws NoMoreLineException S'il n'y a plus de ligne dans le fichier.
     */
    public String nextLine() throws NoMoreLineException {
        if (!hasNextLine() || pos < -1)
            throw new NoMoreLineException("Ligne manquante : " + pos);

        pos++;
        return lineList.get(pos);
    }

    /**
     * Méthode permettant de récupérer une ligne dans le fichier.
     *
     * @param pos Le numéro de la ligne à récupérer.
     * @return La ligne portant le numéro passé en paramètre.
     * @throws NoMoreLineException Si la ligne n'existe pas dans le fichier.
     */
    public String getLine(int pos) throws NoMoreLineException {
        if (pos >= lineList.size() || pos < 0)
            throw new NoMoreLineException("Il n'y a pas ligne " + pos + " dans le fichier (max. " + lineList.size() + ").");

        return lineList.get(pos);
    }

    /**
     * Méthode permettant de récupérer une liste de ligne entre les positions pos1 et pos2 inclus.
     *
     * @param pos1 La première a récupérer.
     * @param pos2 La dernière ligne a récupérer.
     * @return Une liste de ligne entre ps1 et pos2.
     * @throws NoMoreLineException S'il n'est pas possible de récupérer les lignes demander.
     */
    public List<String> getLines(int pos1, int pos2) throws NoMoreLineException {
        if ((pos1 < 0 || pos2 >= lineList.size()) && pos1 <= pos2)
            throw new NoMoreLineException("Les bornes ne sont pas valide (" + pos1 + ", " + pos2 + "), " +
                    "par rapport au fichier (0" + lineList.size() + ").");
        List<String> list = new ArrayList<>();
        for (int i = pos1; i <= pos2; i++) {
            list.add(lineList.get(i));
        }
        return list;
    }

    /**
     * Méthode permettant de récupérer toutes les lignes du fichier.
     *
     * @return La liste de toutes les lignes.
     */
    public List<String> getAllLines() {
        return new ArrayList<>(lineList);
    }

    /**
     * Méthode permettant de positionner le curseur sur la prochaine ligne
     * commençant par la suite de caractères passé en paramètre.
     *
     * @param chars La suite de caractères recherché.
     * @return Vrai s'il y a une ligne qui commence par <code>chars</code>, faux sinon.
     */
    public boolean nextLineStringWith(String chars) {
        for (int i = pos + 1; i < lineList.size(); i++) {
            if (lineList.get(i).startsWith(chars)) {
                pos = i;
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode permettant de positionner le curseur sur la prochaine ligne
     * ne commençant pas par la suite de caractères passé en paramètre.
     *
     * @param chars La liste de caractères.
     * @return Vrai s'il y a bien une ligne correspondante, faux sinon.
     */
    public boolean nextLineNotStartingWith(String chars) {
        for (int i = pos + 1; i < lineList.size(); i++) {
            if (!lineList.get(i).startsWith(chars)) {
                pos = i;
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode permettant de savoir si la ligne suivante commence par la chaine de caractères passé en paramètre.
     *
     * @param chars La chaine de caractères.
     * @return Vrai si la prochaine ligne commence par la chaine de caractères, faux sinon.
     */
    public boolean isNextLineStartsWith(String chars) {
        if (pos + 1 >= lineList.size()) {
            return false;
        }
        return lineList.get(pos + 1).startsWith(chars);
    }

    /**
     * Méthode permettant de savoir s'il y a une nouvelle ligne dans le fichier.
     *
     * @return Vrai s'il y a une nouvelle ligne dans le fichier, faux sinon.
     */
    public boolean hasNextLine() {
        return pos + 1 < lineList.size();
    }

    /**
     * Méthode permettant de récupérer le numéro de la ligne.
     *
     * @return Le numéro de la ligne dans le fichier.
     */
    public int getLineNumber() {
        return pos;
    }

}