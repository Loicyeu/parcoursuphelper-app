/**
 * Package contenant les classes servant a parser un fichier de questions,
 * ou des questions dans un fichier de questions.
 *
 * @author Loïc HENRY
 * @version 1.1.0
 */
package fr.parcoursupHelper.model.parser;