package fr.parcoursupHelper.model.parser;

/**
 * Exception ParseException.
 * symbolise une erreur de parsing.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
public class ParseException extends Exception {

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    ParseException() {
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    ParseException(String message) {
        super(message);
    }
}
