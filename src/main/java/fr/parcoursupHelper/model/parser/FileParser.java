package fr.parcoursupHelper.model.parser;

import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.question.AnswersQuestion;
import fr.parcoursupHelper.model.question.Question;
import fr.parcoursupHelper.model.question.QuestionRegistry;
import fr.parcoursupHelper.model.question.annotation.QuestionParseConstructor;
import fr.parcoursupHelper.model.question.annotation.QuestionParseField;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Classe FileParser.<br>
 * Permet de parser le fichier de questions.
 *
 * @author Loïc HENRY
 * @version 1.1.0
 */
public final class FileParser {

    private final Reader reader;
    private List<String> exceptions;

    /**
     * Constructeur de la classe FileParser.<br>
     *
     * @param file Le fichier de questions a parse.
     * @throws FileNotFoundException Si le fichier n'existe pas.
     */
    public FileParser(File file) throws FileNotFoundException {
        this.reader = new Reader(file);
        this.exceptions = new ArrayList<>();
    }

    /**
     * Méthode permettant de récupérer la liste des questions avec leurs réponses, détaillés dans le fichier.
     * Remet a zéros la liste des exceptions.
     *
     * @return La liste des questions détaillés dans le fichier.
     * @see #parseQuestion(Map)
     * @see #parseAnswer()
     */
    public final List<Question> parse() {
        List<Question> questionList = new ArrayList<>();
        this.exceptions = new ArrayList<>();
        Map<String, String> questionParams = new HashMap<>();

        while (reader.hasNextLine()) {
            List<Answer> answerList = new ArrayList<>();
            try {
                if (reader.nextLineStringWith("question:")) {
                    while (reader.hasNextLine() && reader.isNextLineStartsWith("\t")) {
                        String line = reader.nextLine();
                        if (line.trim().startsWith("answer:")) {
                            answerList.add(parseAnswer());
                        } else {
                            String[] split = line.split(":");
                            questionParams.put(split[0].trim().toLowerCase(Locale.FRANCE), split[1].trim());
                        }
                    }

                    if (questionParams.keySet().containsAll(List.of("name", "label", "weighting", "type"))) {
                        Question question = parseQuestion(questionParams);
                        if (question instanceof AnswersQuestion) {
                            ((AnswersQuestion) question).addAnswers(answerList);
                        }
                        questionList.add(question);
                    } else {
                        throw new ParseException("Missing parameter 'name', 'label', 'weighting', or 'type' in the file.");
                    }
                } else {
                    throw new ParseException("Missing 'question:' tag at line " + reader.getLineNumber());
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                exceptions.add("Le paramètre de la ligne " + reader.getLineNumber() + " est invalide.");
            } catch (ParseException e) {
                exceptions.add(e.getMessage());
            } catch (ReflectiveOperationException e) {
                exceptions.add("CRITICAL : " + e.getMessage());
            }
        }
        return questionList;
    }

    /**
     * Méthode permettant de récupérer la liste des exceptions après avoir parse un fichier.
     *
     * @return La liste des exceptions.
     */
    public final List<String> getExceptions() {
        return new ArrayList<>(exceptions);
    }

    /**
     * Méthode permettant de parse une réponse dans le fichier.
     *
     * @return La réponse parser.
     * @throws ParseException Si le fichier de questions n'est pas valide.
     */
    private Answer parseAnswer() throws ParseException {
        String line = reader.nextLine();
        if (line.startsWith("\t\t")) {
            String answerLabel = line.split(":")[1].trim();
            line = reader.nextLine();
            if (line.startsWith("\t\t")) {
                int answerValue = Integer.parseInt(line.split(":")[1].trim());
                return new Answer(answerLabel, answerValue);
            } else {
                throw new ParseException("Parsing error at line (" + reader.getLineNumber() + ")");
            }
        } else {
            throw new ParseException("Parsing error at line (" + reader.getLineNumber() + ")");
        }
    }

    /**
     * Méthode permettant de parser une question dans le fichier.
     *
     * @param questionParams La map contenant toutes les (clé, valeur) du fichier, correspondant à la question.
     * @return La question parser.
     * @throws ParseException               Si le fichier de questions n'est pas valide.
     * @throws ReflectiveOperationException Si une erreur se produit lors de l'instanciation de la question.
     */
    private Question parseQuestion(Map<String, String> questionParams)
            throws ParseException, ReflectiveOperationException {
        Constructor<?> classConstructor = null;
        QuestionParseConstructor questionParseConstructor = null;

        Class<?> cl = QuestionRegistry.getQuestionParse(questionParams.get("type"));
        for (Constructor<?> constructor : cl.getConstructors()) {
            QuestionParseConstructor constructorAnnotation = constructor.getAnnotation(QuestionParseConstructor.class);
            if (constructorAnnotation != null) {
                classConstructor = constructor;
                questionParseConstructor = constructorAnnotation;
                break;
            }
        }
        if (classConstructor == null) {
            throw new ParseException("Unexpected error ! Cannot retrieve question's constructor");
        }

        for (Field field : cl.getDeclaredFields()) {
            QuestionParseField questionField = field.getAnnotation(QuestionParseField.class);
            if (questionField != null) {
                if (questionParams.containsKey(questionField.name())) {
                    questionParams.put(field.getName(), questionParams.get(questionField.name()));
                    questionParams.remove(questionField.name());
                } else {
                    throw new ParseException("Missing param '" + questionField.name() + "" +
                            "' with question type : " + questionParams.get("type"));
                }
            }
        }

        List<Object> paramsList = new ArrayList<>();
        for (String parameterName : questionParseConstructor.paramsName()) {
            paramsList.add(questionParams.get(parameterName));
        }

        return (Question) classConstructor.newInstance(paramsList.toArray(new Object[0]));
    }
}