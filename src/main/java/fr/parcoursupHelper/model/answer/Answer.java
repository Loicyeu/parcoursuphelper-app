package fr.parcoursupHelper.model.answer;

import fr.loicyeu.dao.FieldType;
import fr.loicyeu.dao.annotations.DaoField;
import fr.loicyeu.dao.annotations.DaoTable;
import fr.loicyeu.dao.annotations.PrimaryKey;
import fr.parcoursupHelper.model.question.AnswersQuestion;
import fr.parcoursupHelper.model.utils.NumberTextField;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Classe Answer.<br>
 *
 * @author Timothé CABON, Loïc HENRY
 * @version 1.1.0
 */
@DaoTable(tableName = "Answer")
public class Answer {

    private static int instanceNumber;

    @DaoField(name = "id", type = FieldType.INT)
    private final int id;

    @DaoField(name = "uid", type = FieldType.VARCHAR)
    @PrimaryKey
    private String uid;

    @DaoField(name = "label", type = FieldType.VARCHAR)
    private String label;

    @DaoField(name = "value", type = FieldType.INT)
    private int value;

    @DaoField(name = "isSelected", type = FieldType.BOOLEAN)
    private boolean isSelected;

    private AnswersQuestion question;

    /**
     * Constructeur de la classe Answer.<br>
     *
     * @param label    Le libellé de la réponse.
     * @param value    La valeur de question.
     * @param question La question a laquelle réfère la réponse.
     */
    public Answer(String label, int value, AnswersQuestion question) {
        this.label = label;
        this.value = value;
        this.isSelected = false;
        this.question = question;
        this.id = instanceNumber;
        if (question != null) {
            setUid(question.getUid());
        } else {
            this.uid = "ANSWER-" + instanceNumber;
        }
        instanceNumber++;
    }

    /**
     * Constructeur de la classe Answer.<br>
     * Par défaut la question a laquelle réfère cette réponse est null.
     *
     * @param label Le libellé de la réponse.
     * @param value La valeur de question.
     */
    public Answer(String label, int value) {
        this(label, value, null);
    }

    /**
     * Constructeur de la classe Answer.<br>
     * Par défaut le libellé de la réponse est <code>"Réponse x"</code> ou <code>x</code> est le numéro de la réponse.
     *
     * @param value    La valeur de question.
     * @param question La question a laquelle réfère la réponse.
     */
    public Answer(int value, AnswersQuestion question) {
        this("Réponse " + instanceNumber, value, question);
    }

    /**
     * Constructeur de la classe Answer.<br>
     * Permet d'obtenir une copie de la classe Answer rattaché a une nouvelle question.
     *
     * @param answer   La réponse a copier.
     * @param question La question référente de la copie de la réponse.
     */
    public Answer(Answer answer, AnswersQuestion question) {
        this.label = answer.label;
        this.value = answer.value;
        this.isSelected = answer.isSelected;
        this.question = question;
        this.id = answer.id;
        setUid(question.getUid());
    }

    /**
     * Constructeur privée pour le DAO.
     */
    private Answer() {
        this("ANSWER-" + instanceNumber, 0);
    }

    /**
     * Méthode static permettant de réinitialisé le compteur d'instance de la classe Answer.
     */
    public static void resetInstanceNumber() {
        instanceNumber = 0;
    }

    /**
     * Permet de modifier l'uid de la réponse en fonction de la question a laquelle elle est rattaché.
     *
     * @param uid L'uid de la question a laquelle elle est rattaché.
     */
    public void setUid(String uid) {
        this.uid = "ANSWER-" + id + uid;
    }

    /**
     * Méthode permettant de récupérer le libellé de la réponse.
     *
     * @return Le libellé de la réponse.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Méthode permettant de modifier le label de la réponse.
     *
     * @param label Le nouveau label.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Méthode permettant de récupérer la valeur de la réponse.
     *
     * @return La valeur de la réponse.
     */
    public int getValue() {
        return value;
    }

    /**
     * Méthode permettant de modifier la valeur de la réponse.
     *
     * @param value Le nouveau label.
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Méthode permettant de savoir si la réponse est sélectionné.
     *
     * @return Vrai si la question sélectionnée, faux sinon.
     */
    public boolean isSelected() {
        if (question == null)
            throw new IllegalStateException("La réponse n'est associée à aucune question.");
        return isSelected;
    }

    /**
     * Méthode permettant de modifier si la réponse est sélectionné ou non.
     *
     * @param selected Vrai si la réponse est sélectionné, faux sinon.
     */
    public void setSelected(boolean selected) {
        if (question == null)
            throw new IllegalStateException("La réponse n'est associée à aucune question.");
        isSelected = selected;
    }

    /**
     * Méthode permettant de récupérer la question référente de la réponse.
     *
     * @return La question référente de la question.
     */
    public AnswersQuestion getQuestion() {
        return question;
    }

    /**
     * Méthode permettant de modifier la question référente de la réponse.
     *
     * @param question La nouvelle question référente de la réponse.
     */
    public void setQuestion(AnswersQuestion question) {
        this.question = question;
    }

    /**
     * Méthode permettant de récupérer une VBox contenant la vue permettant de modifier la dite réponse.
     * Les nodes présent dans la VBox doivent avoir des listener d'évènement permettant de mettre a jour la réponse.
     *
     * @return La VBox contenant tous les éléments permettant de modifier la réponse.
     */
    public VBox getCreatingDisplayable() {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.setPadding(new Insets(10d));
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);

        HBox hBoxLabel = new HBox();
        hBoxLabel.setAlignment(Pos.CENTER_LEFT);
        hBoxLabel.setMaxWidth(Double.MAX_VALUE);
        hBoxLabel.setPadding(new Insets(0, 0, 10, 0));
        Label labelLabel = new Label("Libellé : ");
        labelLabel.setMaxHeight(Double.MAX_VALUE);
        labelLabel.setAlignment(Pos.CENTER_LEFT);
        TextField labelField = new TextField(label);
        labelField.setPromptText("Réponse ...");
        labelField.setMaxWidth(Double.MAX_VALUE);
        labelField.setOnKeyTyped(event -> this.label = labelField.getText());
        hBoxLabel.getChildren().addAll(labelLabel, labelField);

        HBox hBoxValue = new HBox();
        hBoxValue.setAlignment(Pos.CENTER_LEFT);
        hBoxValue.setMaxWidth(Double.MAX_VALUE);
        hBoxValue.setPadding(new Insets(0, 0, 10, 0));
        Label valueLabel = new Label("Valeur : ");
        valueLabel.setMaxHeight(Double.MAX_VALUE);
        valueLabel.setAlignment(Pos.CENTER_LEFT);

        NumberTextField valueField = new NumberTextField(this.value);
        valueField.setOnKeyTyped(event -> this.value = Integer.parseInt(valueField.getText()));

        hBoxValue.getChildren().addAll(valueLabel, valueField);

        vBox.getChildren().addAll(hBoxLabel, hBoxValue);
        return vBox;
    }

    @Override
    public String toString() {
        return label;
    }
}
