package fr.parcoursupHelper.model.mark;

import fr.parcoursupHelper.model.utils.NumberTextField;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class MarkSystem {

    List<Mark> markList;
    String name;

    public MarkSystem(String name) {
        this.name = name;
        this.markList = new ArrayList<>();
    }

    public List<Mark> getMarkList() {
        return markList;
    }

    public void setMarkList(List<Mark> markList) {
        this.markList = markList;
    }

    public void addMark(Mark mark) {
        this.markList.add(mark);
    }

    public void removeMark(Mark mark) {
        this.markList.remove(mark);
    }

    public void removeMark(String string) throws MarkNotFoundException {
        for (int i = 0; i < markList.size(); i++) {
            if (markList.get(i).getEquivalence().equals(string)) {
                markList.remove(i);
                return;
            } else if (markList.get(i).getValue().equals(string)) {
                markList.remove(i);
                return;
            }
        }
        throw new MarkNotFoundException();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public VBox getCreatingDisplayable(Button ajouterNotation) {

        List<Mark> marklist = this.getMarkList();

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(25, 20, 20, 20));
        vBox.setAlignment(Pos.TOP_CENTER);

        if (!marklist.isEmpty()) {
            HBox typeHbox1 = new HBox();
            typeHbox1.setPadding(new Insets(0, 0, 10, 0));
            typeHbox1.setAlignment(Pos.CENTER);

            Label label1 = new Label("  Equivalence     ");
            Label label2 = new Label("                Valeur numérique");

            typeHbox1.getChildren().addAll(label1, label2);
            vBox.getChildren().add(typeHbox1);
        }

        for (int i = 0; i < marklist.size(); i++) {
            HBox typeHbox = new HBox();
            typeHbox.setPadding(new Insets(0, 0, 10, 0));
            typeHbox.setAlignment(Pos.CENTER);

            TextField equivalence = new TextField(marklist.get(i).getEquivalence());
            NumberTextField value = new NumberTextField();


            int finalI = i;


            equivalence.textProperty().addListener((observable, oldValue, newValue) -> marklist.get(finalI).setEquivalence(equivalence.getText()));

            value.textProperty().addListener((observable, oldValue, newValue) -> marklist.get(finalI).setValue(value.getText()));

            value.appendText(marklist.get(i).getValue());
            typeHbox.getChildren().addAll(equivalence, value);
            vBox.getChildren().add(typeHbox);
        }


        ajouterNotation.setAlignment(Pos.CENTER);
        vBox.getChildren().add(ajouterNotation);

        return vBox;
    }

}
