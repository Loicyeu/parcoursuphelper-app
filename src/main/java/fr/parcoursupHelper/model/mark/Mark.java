package fr.parcoursupHelper.model.mark;

public class Mark {

    String equivalence;
    String value;

    public Mark(String equivalence, String value) {
        this.equivalence = equivalence;
        this.value = value;
    }

    public String getValue() {
        return String.valueOf(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEquivalence() {
        return equivalence;
    }

    public void setEquivalence(String equivalence) {
        this.equivalence = equivalence;
    }

}
