package fr.parcoursupHelper.model.mark;

import java.util.ArrayList;
import java.util.List;

public class MarkSystemRegistry {
    static List<MarkSystem> markSystems;

    public MarkSystemRegistry() {
        markSystems = new ArrayList<>();
    }

    public void addMarkSystem(MarkSystem markSystem) {
        markSystems.add(markSystem);
    }

    public void removeMarkSystem(MarkSystem markSystem) {
        markSystems.remove(markSystem);
    }

    public List<MarkSystem> getMarkSystems() {
        return markSystems;
    }
}
