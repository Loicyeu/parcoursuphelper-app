/**
 * Package comportant les questions de l'application.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
package fr.parcoursupHelper.model.question;