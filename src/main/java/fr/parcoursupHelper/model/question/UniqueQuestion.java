package fr.parcoursupHelper.model.question;

import fr.loicyeu.dao.annotations.DaoTable;
import fr.loicyeu.reflections.ClassGrouper;
import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.question.annotation.QuestionDescriptor;
import fr.parcoursupHelper.model.question.annotation.QuestionParseConstructor;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Classe UniqueQuestion.
 * Représente une question a réponse unique.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
@QuestionDescriptor(displayName = "Question à choix unique", parseName = "unique")
@ClassGrouper(groupName = "questionList", packageDest = "fr.parcoursupHelper.model.question")
@DaoTable(tableName = "UniqueQuestion", fetchSuperFields = true)
public class UniqueQuestion extends AnswersQuestion {

    /**
     * Constructeur de la classe UniqueQuestion.<br>
     * Par défaut le nom et le label valent : <code>"Question x"</code> ou <code>x</code> est le numéro de la question,
     * et le poids vaut 1.0.
     */
    public UniqueQuestion() {
        super();
    }

    /**
     * Constructeur de la classe UniqueQuestion.<br>
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    public UniqueQuestion(String name, String label, float weighting) {
        super(name, label, weighting);
    }

    /**
     * Constructeur de la classe UniqueQuestion.<br>
     * Ce constructeur permet au Parser du fichier de question d'instancier la classe sans re-typer les paramètres.
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    @QuestionParseConstructor(paramsName = {"name", "label", "weighting"})
    public UniqueQuestion(String name, String label, String weighting) throws NumberFormatException {
        super(name, label, Float.parseFloat(weighting));
    }

    @Override
    public Question copy() {
        UniqueQuestion question = new UniqueQuestion(this.name, this.label, this.weighting);
        question.state = this.state;
        for (Answer answer : this.answerList) {
            question.addAnswer(new Answer(answer, question));
        }
        return question;
    }

    /**
     * Méthode permettant de récupérer un <code>VBox</code> avec la question prête a être affiché.
     *
     * @return Un <code>VBox</code> contenant la question et tout ce dont elle a besoin pour être affiché.
     */
    @Override
    public VBox getDisplayable() {
        VBox vBox = new VBox();
        Label label = new Label(getLabel() + "\n");
        label.setFont(Font.font(null, FontWeight.BOLD, -1));

        HBox inHBox = new HBox();
        ToggleGroup group = new ToggleGroup();
        for (Answer answer : answerList) {
            RadioButton radio = new RadioButton(answer.getLabel() + " ");
            radio.setToggleGroup(group);
            radio.setSelected(answer.isSelected());
            radio.setOnMouseClicked(event -> {
                answerList.forEach(answer1 -> answer1.setSelected(false));
                answer.setSelected(true);
            });
            inHBox.getChildren().add(radio);
        }
        vBox.getChildren().addAll(label, inHBox);
        return vBox;
    }

    /**
     * Méthode permettant de récupérer la note de la question sur 1.
     * Si la question ne possède aucune réponse, alors retourne 0.
     *
     * @return La note de la question sur 1.
     */
    @Override
    public float getMark() {
        float mark = 0f;
        float maxValue = 0f;

        for (Answer answer : this.answerList) {
            if (answer.isSelected()) {
                mark = answer.getValue();
            }
            if (answer.getValue() > maxValue) {
                maxValue = answer.getValue();
            }
        }
        System.out.println("MARK :" + mark + " MAX:" + maxValue);
        return maxValue == 0 ? 0 : (mark / maxValue);
    }

    /**
     * Méthode permettant de mettre à jour l'état de la question.
     */
    @Override
    public void updateState() {
        for (Answer answer : answerList) {
            if (answer.isSelected()) {
                state = State.DONE;
                return;
            } else {
                state = State.UNTREATED;
            }
        }
    }
}





