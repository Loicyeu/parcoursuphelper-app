package fr.parcoursupHelper.model.question;

import fr.loicyeu.dao.annotations.DaoSuper;
import fr.parcoursupHelper.model.answer.Answer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Classe AnwsersQuestion.
 * Représente une une question ayant des réponses.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
@DaoSuper
public abstract class AnswersQuestion extends Question {

    protected final List<Answer> answerList;

    /**
     * Constructeur de la classe abstraite AnswersQuestion.<br>
     * Par défaut le nom et le label valent : <code>"Question x"</code> ou <code>x</code> est le numéro de la question,
     * et le poids vaut 1.0.
     */
    protected AnswersQuestion() {
        super();
        this.answerList = new ArrayList<>();
    }

    /**
     * Constructeur de la classe abstraite AnswersQuestion.<br>
     *
     * @param name      Le nom familier de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    protected AnswersQuestion(String name, String label, float weighting) {
        super(name, label, weighting);
        this.answerList = new ArrayList<>();
    }


    /**
     * Méthode permettant de récupérer toutes les réponses de la question.
     *
     * @return La liste de toutes les réponses de la question.
     */
    public List<Answer> getAnswers() {
        return new ArrayList<>(answerList);
    }

    /**
     * Méthode permettant d'ajouter une nouvelle réponse a la question.
     *
     * @param answer La réponse à ajouter à la question.
     */
    public void addAnswer(Answer answer) {
        answer.setQuestion(this);
        this.answerList.add(answer);
    }

    /**
     * Méthode permettant d'ajouter une collection de réponse à la question.
     *
     * @param answerList La collection de réponse à ajouter.
     */
    public void addAnswers(Collection<Answer> answerList) {
        answerList.forEach(this::addAnswer);
    }

    /**
     * Méthode permettant de supprimer une réponse à la question.
     *
     * @param answer La réponse a supprimer.
     */
    public void removeAnswer(Answer answer) {
        answerList.remove(answer);
    }
}
