package fr.parcoursupHelper.model.question;

import fr.parcoursupHelper.model.question.annotation.QuestionDescriptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe QuestionRegistry.
 * Permet de lister toutes les questions qui doivent être utilisés par l'application.
 * Permettant ainsi de récupérer une question a partir d'une chaine de caractère ou à partir d'une question de récupérer
 * sa chaine de caractères correspondante.
 *
 * @author Loïc HENRY
 * @version 1.1.0
 * @see QuestionDescriptor
 * @see fr.loicyeu.reflections.ClassGrouper ClassGrouper
 */
public final class QuestionRegistry {

    private static final Map<Class<?>, String> QUESTION_MAP;
    private static final Map<String, Class<?>> REVERSED_QUESTION_MAP;
    private static final Map<Class<?>, String> QUESTION_PARSE_MAP;
    private static final Map<String, Class<?>> REVERSED_QUESTION_PARSE_MAP;

    private QuestionRegistry() {
    }

    static {
        QUESTION_MAP = new HashMap<>();
        REVERSED_QUESTION_MAP = new HashMap<>();
        QUESTION_PARSE_MAP = new HashMap<>();
        REVERSED_QUESTION_PARSE_MAP = new HashMap<>();

        for(QuestionListRegistry question : QuestionListRegistry.values()) {
            QuestionDescriptor questionDescriptor = question.getClazz().getAnnotation(QuestionDescriptor.class);
            if (questionDescriptor != null) {
                QUESTION_MAP.put(question.getClazz(), questionDescriptor.displayName());
                REVERSED_QUESTION_MAP.put(questionDescriptor.displayName(), question.getClazz());

                if(!questionDescriptor.parseName().trim().equalsIgnoreCase("")) {
                    QUESTION_PARSE_MAP.put(question.getClazz(), questionDescriptor.parseName());
                    REVERSED_QUESTION_PARSE_MAP.put(questionDescriptor.parseName(), question.getClazz());
                }
            }
        }
    }

    /**
     * Méthode permettant de récupérer le nom d'affichage d'une question a partir de sa classe.
     * @param clazz La classe de la question.
     * @return Le nom d'affichage, ou {@code null} si la question n'est pas trouvé.
     */
    public static String getQuestionName(Class<? extends Question> clazz) {
        return QUESTION_MAP.get(clazz);
    }

    /**
     * Méthode permettant de récupérer la liste des noms d'affichage des questions.
     *
     * @return La liste des nom d'affichage des questions qui doivent être utilisé par l'application.
     */
    public static List<String> getQuestionName() {
        return new ArrayList<>(REVERSED_QUESTION_MAP.keySet());
    }

    /**
     * Méthode permettant de récupérer la classe d'une question à partir de son nom d'affichage.
     *
     * @param displayName Le nom d'affichage de la question.
     * @return La classe correspondante ou null si elle n'existe pas.
     */
    public static Class<?> getQuestionClass(String displayName) {
        return REVERSED_QUESTION_MAP.get(displayName);
    }

    /**
     * Méthode permettant de récupérer le nom de parse de la question.
     * @param clazz La classe de la question.
     * @return Le nom de parse de la question, ou {@code null} si la question n'est pas trouvé.
     */
    public static String getQuestionParseName(Class<? extends Question> clazz) {
        return QUESTION_PARSE_MAP.get(clazz);
    }

    /**
     * Méthode permettant de récupérer la classe d'une question à partir de son nom de parse.
     *
     * @param parseName Le nom de parse de la question.
     * @return La classe correspondante ou null si elle n'existe pas.
     */
    public static Class<?> getQuestionParse(String parseName) {
        return REVERSED_QUESTION_PARSE_MAP.get(parseName);
    }
}
