package fr.parcoursupHelper.model.question.annotation;

import java.lang.annotation.*;

/**
 * Annotation QuestionParseField.<br>
 * Doit-être ajouté à toutes les classes qui ont l'annotation {@code QuestionDescriptor} avec le champ {@code parseName}
 * remplis. La classe portant ces annotations pourrons alors être instancié par le bias du fichier de questions.
 * Cette annotation permet de donner un nom plus simple aux attributs de la classe servant dans le fichier de questions.
 * Il faut également donner le nom de la classe dans l'annotation, ce nom est celui qui a été mis dans l'annotation
 * {@code QuestionDescriptor}.
 * Exemple d'utilisation :
 * <pre>{@code
 *      @QuestionDescriptor(
 *          displayName = "Question à curseur coulissant",
 *          parseName = "slider"
 *      )
 *      public class SliderQuestion extends Question {
 *
 *          @QuestionParseField(name = "min", referringClass = "slider")
 *          private int sliderMin;
 *          @QuestionParseField(name = "max", referringClass = "slider")
 *          private int sliderMax;
 *          @QuestionParseField(name = "step", referringClass = "slider")
 *          private float sliderStep;
 *
 *          ...
 *
 *      }
 * }</pre>
 * Le fichier de questions ressemblera alors à cela :
 * <pre>{@code
 *      question:
 *          ...
 *          type: slider
 *          min: 0
 *          max: 20
 *          step: 0.5
 * }</pre>
 *
 * L'utilisation de cette annotation va de pair avec QuestionDescriptor et QuestionParseConstructor qui doivent
 * être respectivement mis sur la classe et les attributs utiles de la classe.
 *
 * @author Loïc HENRY
 * @version 1.1.0
 * @see QuestionDescriptor
 * @see QuestionParseConstructor
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestionParseField {

    String name();
    String referringClass();

}