package fr.parcoursupHelper.model.question.annotation;

import java.lang.annotation.*;

/**
 * Annotation QuestionParseConstructor.<br>
 * Doit-être ajouté à toutes les classes qui ont l'annotation {@code QuestionDescriptor} avec le champ {@code parseName}
 * remplis. La classe portant ces annotations pourrons alors être instancié par le bias du fichier de questions.
 * Cette annotation permet de retrouver le constructeur permettant d'instancier la classe. Ce constructeur doit prendre
 * le plus de paramètres possible et doivent tous être du type {@code String}, il conviendra de les re-typer dans le
 * constructeur et de jeter une exception dans le cas ou le re-typage n'aura pas fonctionné.<br>
 * Il est également nécessaire de préciser dans l'annotation le nom des paramètres du constructeurs dans le bon ordre,
 * le nom des paramètres doit être celui par le parser.
 * Exemple d'utilisation :
 * <pre>{@code
 *      @QuestionDescriptor(
 *          displayName = "Question à curseur coulissant"
 *          parseName = "slider"
 *      )
 *      public class SliderQuestion extends Question {
 *
 *          @QuestionParseConstructor(
 *              paramsName = {"name", "label", "weighting",
 *                  "sliderMin", "sliderMax", "sliderStep"}
 *          )
 *          public SliderQuestion(String name, String label, String weighting,
 *                  String min, String max, String step) {
 *              ...
 *          }
 *
 *      }
 * }</pre>
 *
 * L'utilisation de cette annotation va de pair avec QuestionDescriptor et QuestionParseField qui doivent
 * être respectivement mis sur la classe et les attributs utiles de la classe.
 *
 * @author Loïc HENRY
 * @version 1.2.0
 * @see QuestionDescriptor
 * @see QuestionParseField
 */
@Documented
@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestionParseConstructor {

    String[] paramsName();

}