package fr.parcoursupHelper.model.question.annotation;

import java.lang.annotation.*;

/**
 * Annotation QuestionDescriptor.<br>
 * Doit-être ajouté a toutes les sous-classes de {@link fr.parcoursupHelper.model.question.Question Question}
 * qui doivent être utilisé par l'application en tant que question utilisable.
 * Par défaut le nom d'affichage est "Question" mais peut être changé
 * lors de l'utilisation de l'annotation sur une classe.<br>
 * Exemple d'utilisation :
 * <pre>{@code
 *      @QuestionDescriptor(displayName = "Question à curseur coulissant")
 *      public class SliderQuestion extends Question {
 *          ...
 *      }
 * }</pre>
 * Dans le cas ou la question devra être prise en charge par le parser de questions et de fichier,
 * le champ {@code parseName} devra être remplis avec un simple et <b>unique</b> permettant ainsi d'identifier quel type
 * de question doit être instancié. L'utilisation de ce champ <b>doit</b> nécessite l'utilisation de
 * {@link QuestionParseConstructor} et {@link QuestionParseField} qui doivent être respectivement mis sur
 * le constructeur et les attributs utiles de la classe.
 * Exemple d'utilisation :
 * <pre>{@code
 *      @QuestionDescriptor(
 *          displayName = "Question à curseur coulissant",
 *          parseName = "slider"
 *      )
 *      public class SliderQuestion extends Question {
 *          ...
 *      }
 * }</pre>
 *
 * La classe {@link fr.parcoursupHelper.model.question.MultipleQuestion MultipleQuestion} utilise cette annotation par exemple.
 *
 * @author Loïc HENRY
 * @version 2.0.0
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface QuestionDescriptor {

    String displayName() default "Question";
    String parseName() default "";

}