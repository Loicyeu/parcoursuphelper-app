package fr.parcoursupHelper.model.question;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Questions implements Iterable<Question> {

    private final List<Question> questionList;

    /**
     * Constructeur permettant de créer l'objet Questions a partir d'une List de questions.
     *
     * @param questionList La liste de questions.
     */
    public Questions(List<Question> questionList) {
        this.questionList = questionList;
    }

    /**
     * Constructeur permettant de créer l'objet Questions.
     */
    public Questions() {
        this(new ArrayList<>());
    }

    /**
     * Méthode permettant de récupérer une question à un index précis.
     *
     * @param index L'index du candidat dans la liste.
     * @return La question à l'index souhaité.
     */
    public Question get(int index) {
        return questionList.get(index);
    }

    /**
     * Méthode retournant le nombre de questions présentes dans la liste.
     *
     * @return Le nombre de questions dans la liste.
     */
    public int getSize() {
        return questionList.size();
    }

    /**
     * Méthode permettant d'ajouter une questions à la liste.
     *
     * @param question La question à ajouter.
     */
    public void add(Question question) {
        questionList.add(question);
    }

    /**
     * Méthode permettant de supprimer une questions de la liste.
     *
     * @param question La questions à supprimer.
     * @return Vrai si la questions à bien été supprimé, faux sinon.
     */
    public boolean remove(Question question) {
        return questionList.remove(question);
    }

    /**
     * Méthode permettant de récupérer la liste des questions.
     *
     * @return La liste des questions.
     */
    public List<Question> toList() {
        return questionList;
    }

    /**
     * Méthode permettant d'obtenir une ObservableList de toutes les questions.
     *
     * @return Une ObservableList des questions.
     */
    public ObservableList<Question> toObservableList() {
        return FXCollections.observableArrayList(questionList);
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Question> iterator() {
        return questionList.iterator();
    }
}
