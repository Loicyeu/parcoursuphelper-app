package fr.parcoursupHelper.model.question;

import fr.loicyeu.dao.annotations.DaoTable;
import fr.loicyeu.reflections.ClassGrouper;
import fr.parcoursupHelper.model.answer.Answer;
import fr.parcoursupHelper.model.question.annotation.QuestionDescriptor;
import fr.parcoursupHelper.model.question.annotation.QuestionParseConstructor;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Classe MultipleQuestion.
 * Représente une question a réponse multiple.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
@QuestionDescriptor(displayName = "Question à choix multiples", parseName = "multiple")
@ClassGrouper(groupName = "questionList", packageDest = "fr.parcoursupHelper.model.question")
@DaoTable(tableName = "MultipleQuestion", fetchSuperFields = true)
public class MultipleQuestion extends AnswersQuestion {

    /**
     * Constructeur de la classe MultipleQuestion.<br>
     * Par défaut le nom et le label valent : <code>"Question x"</code> ou <code>x</code> est le numéro de la question,
     * et le poids vaut 1.0.
     */
    public MultipleQuestion() {
        super();
    }

    /**
     * Constructeur de la classe MultipleQuestion.<br>
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    public MultipleQuestion(String name, String label, float weighting) {
        super(name, label, weighting);
    }

    /**
     * Constructeur de la classe MultipleQuestion.<br>
     * Ce constructeur permet au Parser du fichier de question d'instancier la classe sans re-typer les paramètres.
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    @QuestionParseConstructor(paramsName = {"name", "label", "weighting"})
    public MultipleQuestion(String name, String label, String weighting) throws NumberFormatException {
        super(name, label, Float.parseFloat(weighting));
    }

    @Override
    public Question copy() {
        MultipleQuestion question = new MultipleQuestion(this.name, this.label, this.weighting);
        question.state = this.state;
        for (Answer answer : this.answerList) {
            question.addAnswer(new Answer(answer, question));
        }
        return question;
    }

    /**
     * Méthode permettant de récupérer une <code>VBox</code> avec la question prête a être affiché.
     *
     * @return Un <code>VBox</code> contenant la question et tout ce dont elle a besoin pour être affiché.
     */
    @Override
    public VBox getDisplayable() {
        VBox vBox = new VBox();
        HBox inHBox = new HBox();
        Label label = new Label(getLabel() + "\n");
        label.setFont(Font.font(null, FontWeight.BOLD, -1));

        vBox.getChildren().add(label);
        for (Answer answer : answerList) {
            CheckBox checkBox = new CheckBox(answer.getLabel() + " ");
            checkBox.setSelected(answer.isSelected());
            checkBox.setOnMouseClicked(event -> answer.setSelected(checkBox.isSelected()));
            inHBox.getChildren().add(checkBox);
        }
        vBox.getChildren().add(inHBox);
        return vBox;
    }

    /**
     * Méthode permettant de récupérer la note de la question sur 1.
     * Si la question ne possède aucune réponse, getMark renvoie 0.
     *
     * @return La note de la question sur 1.
     */
    @Override
    public float getMark() {
        if (answerList.isEmpty()) {
            return 0f;
        }

        float mark = 0f;
        float maxValue = 0f;

        for (Answer answer : this.answerList) {
            if (answer.isSelected()) {
                mark += answer.getValue();
            }
            maxValue += answer.getValue();
        }
        return mark / maxValue;
    }

    /**
     * Méthode permettant de mettre à jour l'état de la question.
     */
    @Override
    public void updateState() {
        this.state = State.DONE;
    }

}
