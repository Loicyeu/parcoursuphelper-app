package fr.parcoursupHelper.model.question;

import fr.loicyeu.dao.FieldType;
import fr.loicyeu.dao.annotations.DaoField;
import fr.loicyeu.dao.annotations.DaoSuper;
import fr.loicyeu.dao.annotations.PrimaryKey;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.Objects;

/**
 * Classe Question.<br>
 * Représente une question posé par le professeur sur l'application.
 *
 * @author Loïc HENRY, Timothé CABON
 * @version 2.0.0
 */
@DaoSuper
public abstract class Question {

    protected static int instanceNumber;

    @DaoField(name = "id", type = FieldType.INT)
    protected final int id;
    @DaoField(name = "uid", type = FieldType.VARCHAR)
    @PrimaryKey
    protected String uid;

    @DaoField(name = "name", type = FieldType.VARCHAR)
    protected String name;
    @DaoField(name = "label", type = FieldType.VARCHAR)
    protected String label;
    @DaoField(name = "weighting", type = FieldType.FLOAT)
    protected float weighting;

    protected State state;

    /**
     * Constructeur de la classe abstraite Question. <br>
     * Par défaut le nom et le label valent : <code>"Question x"</code> ou <code>x</code> est le numéro de la question,
     * et le poids vaut 1.0.
     */
    Question() {
        this("Question " + instanceNumber, "Question " + instanceNumber, 1f);
    }

    /**
     * Constructeur de la classe abstraite Question. <br>
     *
     * @param name      Le nom familier de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    Question(String name, String label, float weighting) {
        this.name = name;
        this.label = label;
        this.weighting = weighting;
        this.state = State.UNTREATED;
        this.id = instanceNumber;
        this.uid = "QUESTION-" + instanceNumber;
        instanceNumber++;
    }

    /**
     * Méthode qui permet de réinitialiser le compteur d'instance de la classe Question
     */
    public static void resetInstanceNumber() {
        instanceNumber = 0;
    }

    /**
     * Méthode permettant de récupérer le label de la question.
     *
     * @return Le label de la question.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Méthode permettant de changer le label de la question.
     *
     * @param label Le nouveau label de la question.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Méthode permettant de récupérer le nom de la question.
     *
     * @return Le nom de la question.
     */
    public String getName() {
        return name;
    }

    /**
     * Méthode permettant de changer le nom de la question.
     *
     * @param name Le nouveau nom de la question.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Méthode permettant de récupérer le poids de la question.
     *
     * @return Le poids de la question.
     */
    public float getWeighting() {
        return weighting;
    }

    /**
     * Méthode permettant de changer le poids de la question.
     *
     * @param weighting Le nouveau poids de la question.
     */
    public void setWeighting(float weighting) {
        this.weighting = weighting;
    }

    /**
     * Méthode permettant de mettre à jour l'état de la question.
     */
    public abstract void updateState();

    /**
     * Méthode permettant de récupérer l'état de la question.
     *
     * @return L'état de la question.
     */
    public State getState() {
        return state;
    }

    public abstract Question copy();

    /**
     * Méthode permettant de récupérer un <code>VBox</code> avec la question prête a être affiché.
     *
     * @return Un <code>VBox</code> contenant la question et tout ce dont elle a besoin pour être affiché.
     */
    public abstract VBox getDisplayable();

    /**
     * Méthode permettant de récupérer une VBox contenant la vue permettant de modifier la dite question.
     * Les nodes présent dans la VBox doivent avoir des listener d'évènement permettant de mettre a jour la question.
     *
     * @return La VBox contenant tous les éléments permettant de modifier la question.
     */
    public VBox getCreatingDisplayable() {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(25, 20, 20, 20));
        vBox.setAlignment(Pos.TOP_LEFT);

        HBox typeHbox = new HBox();
        typeHbox.setPadding(new Insets(0, 0, 10, 0));
        typeHbox.setAlignment(Pos.CENTER);
        Label typeLabel = new Label(QuestionRegistry.getQuestionName(getClass()));
        typeLabel.setFont(Font.font(null, FontWeight.BOLD, typeLabel.getFont().getSize()));
        typeHbox.getChildren().add(typeLabel);

        HBox nameHbox = new HBox();
        nameHbox.setPadding(new Insets(0, 0, 10, 0));
        Label nameLabel = new Label("Nom familier de la question : ");
        nameLabel.setMaxHeight(Integer.MAX_VALUE);
        TextField nameField = new TextField(name);
        nameField.setPromptText("Notes anglais");
        nameField.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(nameField, Priority.ALWAYS);
        nameField.setOnKeyTyped(event -> this.name = nameField.getText());
        nameHbox.getChildren().addAll(nameLabel, nameField);

        HBox labelHbox = new HBox();
        labelHbox.setPadding(new Insets(0, 0, 10, 0));
        Label labelLabel = new Label("Libellé de la question : ");
        labelLabel.setMaxHeight(Integer.MAX_VALUE);
        TextField labelField = new TextField(label);
        labelField.setPromptText("Quelle est la moyenne d'anglais ?");
        labelField.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(labelField, Priority.ALWAYS);
        labelField.setOnKeyTyped(event -> this.label = labelField.getText());
        labelHbox.getChildren().addAll(labelLabel, labelField);

        HBox valueHbox = new HBox();
        valueHbox.setPadding(new Insets(0, 0, 10, 0));
        Label valueLabel1 = new Label("Coefficient de la question : ");
        valueLabel1.setMaxHeight(Integer.MAX_VALUE);
        TextField valueField = new TextField(weighting + "");
        valueField.setPromptText("2.");
        valueField.setPrefWidth(64d);
        valueField.setOnKeyTyped(event -> {
            try {
                this.weighting = Float.parseFloat(valueField.getText());
            } catch (Exception ignored) {
                //TODO
            }
        });
        Label valueLabel2 = new Label(" pts");
        valueLabel2.setPrefHeight(25.0);
        valueHbox.getChildren().addAll(valueLabel1, valueField, valueLabel2);

        vBox.getChildren().addAll(typeHbox, nameHbox, labelHbox, valueHbox);
        return vBox;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String applicantId) {
        this.uid = applicantId + "QUESTION" + id;
    }

    public int getId() {
        return id;
    }

    /**
     * Méthode permettant de récupérer la note de la question sur 1.
     *
     * @return La note de la question sur 1.
     */
    public abstract float getMark();

    /**
     * Methode permettant de savoir si deux <code>Question</code> sont égales.<br>
     * Une <code>Question</code> est égale à une autre si leur name et leurs label sont les même sans tenir
     * compte de la casse. Il faut également que les deux questions soit du même type, cet a dire que si une question
     * est une <code>SliderQuestion</code> il faudra que l'autre question le soit aussi en plus du nom et du libellé.
     *
     * @param obj L'<code>Object</code> a tester.
     * @return Vrai si les questions sont égales, faux sinon.
     */
    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof Question) {
            Question question = (Question) obj;
            return question.label.equalsIgnoreCase(this.label)
                    && question.name.equalsIgnoreCase(this.name)
                    && question.getClass().equals(getClass());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, label);
    }

    public enum State {
        DONE,
        UNTREATED
    }

}
