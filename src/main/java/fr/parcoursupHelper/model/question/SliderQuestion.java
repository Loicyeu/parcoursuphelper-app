package fr.parcoursupHelper.model.question;

import fr.loicyeu.dao.FieldType;
import fr.loicyeu.dao.annotations.DaoField;
import fr.loicyeu.dao.annotations.DaoTable;
import fr.loicyeu.reflections.ClassGrouper;
import fr.parcoursupHelper.model.question.annotation.QuestionDescriptor;
import fr.parcoursupHelper.model.question.annotation.QuestionParseConstructor;
import fr.parcoursupHelper.model.question.annotation.QuestionParseField;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Classe SliderQuestion.<br>
 * Représente une question qui peut être répondu par un curseur coulissant.
 *
 * @author Loïc HENRY
 * @version 1.0.0
 */
@QuestionDescriptor(displayName = "Question à curseur coulissant", parseName = "slider")
@ClassGrouper(groupName = "questionList", packageDest = "fr.parcoursupHelper.model.question")
@DaoTable(tableName = "SliderQuestion", fetchSuperFields = true)
public class SliderQuestion extends Question {

    @QuestionParseField(name = "min", referringClass = "slider")
    @DaoField(name = "sliderMin", type = FieldType.INT)
    private int sliderMin;

    @QuestionParseField(name = "max", referringClass = "slider")
    @DaoField(name = "sliderMax", type = FieldType.INT)
    private int sliderMax;

    @QuestionParseField(name = "step", referringClass = "slider")
    @DaoField(name = "sliderStep", type = FieldType.FLOAT)
    private float sliderStep;

    @DaoField(name = "sliderValue", type = FieldType.FLOAT)
    private float sliderValue;

    /**
     * Constructeur de la classe SliderQuestion.<br>
     * Par défaut le nom et le label valent : <code>"Question x"</code> ou <code>x</code> est le numéro de la question,
     * et le poids vaut 1.0.
     * Par défaut, le min est à 0 et le max à 10 et le pas est a 1.0.
     */
    public SliderQuestion() {
        super();
        this.sliderMin = 0;
        this.sliderMax = 10;
        this.sliderStep = 1f;
        this.sliderValue = this.sliderMin;
        this.state = State.DONE;
    }

    /**
     * Constructeur de la classe SliderQuestion.<br>
     * Par défaut le min est à 0 et le max à 10 et le pas est a 1.0.
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     */
    public SliderQuestion(String name, String label, float weighting) {
        this(name,
                label,
                weighting,
                0,
                10
        );
    }

    /**
     * Constructeur de la classe SliderQuestion.<br>
     * Par défaut le pas du slider est à 1.0.
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     * @param sliderMin La valeur minimum du slider.
     * @param sliderMax La valeur maximum du slider.
     */
    public SliderQuestion(String name, String label, float weighting, int sliderMin, int sliderMax) {
        this(name,
                label,
                weighting,
                sliderMin,
                sliderMax,
                1.0f);
    }

    /**
     * Constructeur de la classe SliderQuestion.
     *
     * @param name       Le nom de la question.
     * @param label      Le label de la question.
     * @param weighting  Le poids de la question.
     * @param sliderMin  La valeur minimum du slider.
     * @param sliderMax  La valeur maximum du slider.
     * @param sliderStep La valeur du pas du slider.
     */
    public SliderQuestion(String name, String label, float weighting, int sliderMin, int sliderMax, float sliderStep) {
        super(name, label, weighting);
        this.sliderMin = sliderMin;
        this.sliderMax = sliderMax;
        this.sliderStep = sliderStep;
        this.sliderValue = this.sliderMin;
        this.state = State.DONE;
    }

    /**
     * Constructeur de la classe SliderQuestion.
     * Ce constructeur permet au Parser du fichier de question d'instancier la classe sans re-typer les paramètres.
     *
     * @param name      Le nom de la question.
     * @param label     Le label de la question.
     * @param weighting Le poids de la question.
     * @param min       La valeur minimum du slider.
     * @param max       La valeur maximum du slider.
     * @param step      La valeur du pas du slider.
     */
    @QuestionParseConstructor(paramsName = {"name", "label", "weighting", "sliderMin", "sliderMax", "sliderStep"})
    public SliderQuestion(String name, String label, String weighting, String min, String max, String step) throws NumberFormatException {
        this(name, label, Float.parseFloat(weighting), Integer.parseInt(min), Integer.parseInt(max), Float.parseFloat(step));
    }


    public int getSliderMin() {
        return sliderMin;
    }

    public void setSliderMin(int sliderMin) {
        this.sliderMin = sliderMin;
    }

    public int getSliderMax() {
        return sliderMax;
    }

    public void setSliderMax(int sliderMax) {
        this.sliderMax = sliderMax;
    }

    public float getSliderStep() {
        return sliderStep;
    }

    public void setSliderStep(float sliderStep) {
        this.sliderStep = sliderStep;
    }

    public float getSliderValue() {
        return sliderValue;
    }

    public void setSliderValue(float sliderValue) {
        this.sliderValue = sliderValue;
    }

    /**
     * Méthode permettant de mettre à jour l'état de la question.<br>
     * Ne fait rien.
     */
    @Override
    public void updateState() {
        //Méthode inutile
    }

    @Override
    public Question copy() {
        return new SliderQuestion(name, label, weighting, sliderMin, sliderMax, sliderStep);
    }

    /**
     * Méthode permettant de récupérer un <code>VBox</code> avec la question prête a être affiché.
     *
     * @return Un <code>VBox</code> contenant la question et tout ce dont elle a besoin pour être affiché.
     */
    @Override
    public VBox getDisplayable() {
        VBox vBox = new VBox();
        Label questionName = new Label(getLabel());
        questionName.setFont(Font.font(null, FontWeight.BOLD, -1));

        Slider slider = new Slider(sliderMin, sliderMax, sliderValue);
        slider.setSnapToTicks(true);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(sliderMax / 2f);
        slider.setBlockIncrement((int) ((sliderMax / sliderStep) / 2));
        slider.setMinorTickCount((int) ((sliderMax / sliderStep) / 2));
        slider.valueProperty().addListener((observable, oldValue, newValue) -> this.sliderValue = newValue.floatValue());

        vBox.getChildren().addAll(questionName, slider);
        return vBox;
    }

    /**
     * Méthode permettant de récupérer une VBox contenant la vue permettant de modifier la dite question.
     * Les nodes présent dans la VBox doivent avoir des listener d'évènement permettant de mettre a jour la question.
     *
     * @return La VBox contenant tous les éléments permettant de modifier la question.
     */
    @Override
    public VBox getCreatingDisplayable() {
        VBox vBox = super.getCreatingDisplayable();

        Separator separator = new Separator(Orientation.HORIZONTAL);
        separator.setPadding(new Insets(0, 0, 10, 0));

        HBox minHbox = new HBox();
        minHbox.setPadding(new Insets(0, 0, 10, 0));
        Label minLabel = new Label("Valeur minimale du curseur coulissant : ");
        minLabel.setPrefHeight(25.0);
        TextField minField = new TextField(Integer.toString(sliderMin));
        minField.setPromptText("0");
        minField.setPrefWidth(64d);
        minField.setOnKeyTyped(event -> {
            try {
                this.sliderMin = Integer.parseInt(minField.getText());
            } catch (Exception ignored) {
                //TODO
            }
        });
        minHbox.getChildren().addAll(minLabel, minField);

        HBox maxHbox = new HBox();
        maxHbox.setPadding(new Insets(0, 0, 10, 0));
        Label maxLabel = new Label("Valeur maximale du curseur coulissant : ");
        maxLabel.setPrefHeight(25.0);
        TextField maxField = new TextField(Integer.toString(sliderMax));
        maxField.setPromptText("20");
        maxField.setPrefWidth(64d);
        maxField.setOnKeyTyped(event -> {
            try {
                this.sliderMax = Integer.parseInt(maxField.getText());
            } catch (Exception ignored) {
                //TODO
            }
        });
        maxHbox.getChildren().addAll(maxLabel, maxField);

        HBox stepHbox = new HBox();
        stepHbox.setPadding(new Insets(0, 0, 10, 0));
        Label stepLabel = new Label("Pas du curseur coulissant : ");
        stepLabel.setPrefHeight(25.0);
        TextField stepField = new TextField(Float.toString(sliderStep));
        stepField.setPromptText("0.5");
        stepField.setPrefWidth(64d);
        stepField.setOnKeyTyped(event -> {
            try {
                this.sliderStep = Float.parseFloat(stepField.getText());
            } catch (Exception ignored) {
                //TODO
            }
        });
        stepHbox.getChildren().addAll(stepLabel, stepField);

        vBox.getChildren().addAll(separator, minHbox, maxHbox, stepHbox);
        return vBox;
    }

    /**
     * Méthode permettant de récupérer la note de la question sur 1.
     * Si la valeur maximal du slider est 0, alors retourne 0.
     *
     * @return La note de la question sur 1.
     */
    @Override
    public float getMark() {
        if (this.sliderMax == 0) {
            return 0;
        }

        return this.sliderValue / this.sliderMax;
    }
}











