package fr.parcoursupHelper.model.utils;

import javafx.scene.control.TextField;

public class NumberTextField extends TextField {

    /**
     * Creates a {@code NumberTextField} with empty text content.
     */
    public NumberTextField() {
    }

    /**
     * Creates a {@code NumberTextField} with an initial value content.
     */
    public NumberTextField(int value) {
        super(Integer.toString(value));
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (validate(text)) {
            super.replaceText(start, end, text);
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (validate(text)) {
            super.replaceSelection(text);
        }
    }

    private boolean validate(String text) {
        return text.matches("^[0-9]*\\.?[0-9]*$");
    }
}