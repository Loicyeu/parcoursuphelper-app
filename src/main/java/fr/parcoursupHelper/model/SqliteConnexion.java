package fr.parcoursupHelper.model;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class SqliteConnexion {


    private static SqliteConnexion instance;
    private Connection connection;

    /**
     * Constructeur de la classe SqliteConnexion
     *
     * @param url URL vers la base de données
     */
    private SqliteConnexion(String url) {
        url = url.replace("%20", " ");
        connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + url);

        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Permet d'ouvrir une connexion à une base de donnée externe.
     *
     * @param path Le chemin vers la base de données.
     * @return La connexion SQlite3.
     */
    public static SqliteConnexion getExternalInstance(URL path) {
        return new SqliteConnexion(path.getPath());
    }

    /**
     * Méthode retournant une connexion SQLite avec le fichier passé en paramètre
     *
     * @param path Le chemin vers la base de données.
     * @return La connexion SQLite3
     */
    public static SqliteConnexion getInstance(String path) {
        if (instance == null) {
            instance = new SqliteConnexion(path);
        }
        return instance;
    }

    public Connection getConnexion() {
        return connection;
    }

    public void closeConnection() {
        connection = null;
    }

}