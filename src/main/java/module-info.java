module fr.parcoursupHelper {
    requires transitive javafx.base;
    requires transitive javafx.graphics;

    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires poi;
    requires poi.ooxml;
    requires java.compiler;
    requires fr.loicyeu.reflections;
    requires fr.loicyeu.dao;
    requires sqlite.jdbc;

    exports fr.parcoursupHelper.model.answer;
    exports fr.parcoursupHelper.model.applicant;
    exports fr.parcoursupHelper.model.parser;
    exports fr.parcoursupHelper.model.question;
    exports fr.parcoursupHelper;
    exports fr.parcoursupHelper.model.mark;

    opens fr.parcoursupHelper to javafx.graphics;
    opens fr.parcoursupHelper.controller to javafx.fxml;
    opens fr.parcoursupHelper.model.question to fr.loicyeu.dao;
    opens fr.parcoursupHelper.model.answer to fr.loicyeu.dao;
    opens fr.parcoursupHelper.model.applicant to fr.loicyeu.dao;
}