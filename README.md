![Logo de parcoursupHelper](https://parcoursupHelper.loicyeu.fr/images/pH_logo.svg "Logo de parcoursupHelper")

# parcoursupHelper

ParcoursupHelper est un logiciel qui assiste le professeur pour analyser des dossiers de lycéens candidats pour une
formation. Le professeur pourra alors définir ses propres critères d’évaluation des dossiers. ParcoursupHelper permet
d'accélérer le processus en aidant à la notation des dossiers Parcoursup® contrairement à une version manuelle empirique
qui demande beaucoup de temps. Le logiciel prendra un fichier Excel (contenant des numéros de dossiers Parcoursup®) en
entrée pour ensuite pouvoir classer les candidats en fonction de leur note globale calculée à partir des critères
préalablement renseignés, par les enseignants. Cette note globale est une évaluation “qualitative” qui va compléter les
notes plus factuelles contenues dans le dossier (notes dans les matières de Première et Terminale).

## [Documentation de l'application](https://parcoursuphelper-team.univ-nantes.io/parcoursuphelper-doc/)

La documentation de l'application fournis toutes les informations concernant le téléchargement, l'installation et
l'utilisation de l'application. Il est également possible d'y trouver toutes les informations concernant la contribution
au code de l'application ainsi que les différentes équipes de développement qui l'ont conçu.

## [Release](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases)

### [Version 1.4.4pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.4.4-pre)

La version 1.4.4pre est disponible pour les distributions Windows x64 et Linux x64.

**Patch :**

- Le calcul de la note global des candidats se fait à présent correctement
- Il est possible de ne sélectionner aucune réponse dans une question à choix multiples
- Possibilité d'utiliser des lettres pour la sélection d'une colonne dans la vue OpenExcel

--- 

### [Version 1.4.3pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.4.3-pre)

La version 1.4.3pre est disponible pour les distributions Windows x64 et Linux x64.

**Patch :**

- Il est désormais possible de mettre autant de lignes vides, ou aucune, entre deux questions dans le DSL
- Améliorations mineures des modèles et des contrôleurs.

--- 

### [Version 1.4.2pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.4.2-pre)

La version 1.4.2pre est disponible pour les distributions Windows x64 et Linux x64.

**Patch :**

- Support de la base de donnée dans les images de l'application

--- 

### [Version 1.4.1pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.4.1-pre)

La version 1.4.1pre est disponible pour les distributions Windows x64 et Linux x64.

**Patch :**

- correction extension placeholder vue main
- activation bouton sauvegarder vue application
- simplification menubar vue application

--- 

### [Version 1.4.0pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.4.0-pre)

La version 1.4.0pre est disponible pour la distribution Windows x64.

**Nouveautés :**

- Utilisation de la librairie `fr.loicyeu.dao` pour les DAOs
- Ajout fonctionnalité sauvegarde vue application
- Chargement d'une sauvegarde vue main
- Ajout Système de notation *non intégré*
- Support des commentaires par le DSL
- Ajout boutons candidat "précédent/suivant" vue application
- Ajout compteur de réponses sur le bouton "menu réponses" vue création question

**Patch :**

- prise en compte des saisies vue application
- export des données de nouveau fonctionnel
- calcul de la note global candidat

--- 

### [Version 1.3.1pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.3.1-pre)

La version 1.3.1pre est disponible pour la distribution Windows x64.

**Patch :**

- Modification des réflexions
- Le parseur est désormais opérationnel

--- 

### [Version 1.3.0pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.3.0-pre)

La version 1.3.0pre est disponible pour la distribution Windows x64.

**Nouveautés :**

- Possibilité d'utiliser des fichiers XLSX en plus des XLS.
- Mise a jour du système de réflexion : utilisation de fr.loicyeu.reflexions
- Ajout du parser inversé permettant d'exporter les questions de l'application dans un fichier de question utilisant le
  même DSL que le parser.

--- 

### [Version 1.2.0pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.2.0-pre)

La version 1.2.0pre est disponible pour la distribution Windows x64.

Mise a jour important ajoutant un nouveau Parser de fichier permettant de récupérer dans un fichier .txt respectant un
certain formalisme, n'importe quel type de question. \
Cette version de l'application parcoursupHelper est encore en BETA et de nombreux bugs subsiste.

--- 

### [Version 1.1.0pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.1.0-pre)

La version 1.1.0pre est disponible pour la distribution Windows x64.

--- 

### [Version 1.0.0pre](https://gitlab.univ-nantes.fr/parcoursuphelper-team/parcoursuphelper-app/-/releases/v1.0.0-pre)

La version 1.0.0pre est disponible pour les distributions Windows x64 et Linux x64.
